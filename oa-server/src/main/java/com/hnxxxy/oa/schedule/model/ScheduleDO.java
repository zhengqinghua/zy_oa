package com.hnxxxy.oa.schedule.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;

import java.time.LocalDateTime;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 日程表
 * </p>
 *
 * @author hzh
 * @since 2022-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("schedule")
public class ScheduleDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 日程Id
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 日程标题
     */
    private String title;

    /**
     * 会议地址
     */
    private String address;

    /**
     * 会议类型
     */
    private int meetingId;

    /**
     * 日程开始时间
     */
    private Date beginTime;

    /**
     * 日程结束时间
     */
    private Date endTime;

    /**
     * 日程内容
     */
    private String content;

    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @JsonProperty("create_by")
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 是否私有
     */
    private int isPrivate;


}
