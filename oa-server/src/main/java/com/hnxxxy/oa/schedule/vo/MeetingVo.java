package com.hnxxxy.oa.schedule.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author hzh
 * @data 2022/5/22 15:34
 **/
@Data
public class MeetingVo {
    /**
     * 会议类型Id
     * 1=公司年会 2=机构会议 3=部门会议 4小组会议 5外部合作会议
     */
    private Integer id;

    /**
     * 会议类型名称
     */
    private String name;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;
}
