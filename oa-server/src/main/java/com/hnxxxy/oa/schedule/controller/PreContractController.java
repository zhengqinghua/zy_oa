package com.hnxxxy.oa.schedule.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 预约人表 前端控制器
 * </p>
 *
 * @author hzh
 * @since 2022-05-10
 */
@RestController
@RequestMapping("/preContractDO")
public class PreContractController {

}

