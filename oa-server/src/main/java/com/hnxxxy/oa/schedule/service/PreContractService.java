package com.hnxxxy.oa.schedule.service;

import com.hnxxxy.oa.schedule.model.PreContractDO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 预约人表 服务类
 * </p>
 *
 * @author hzh
 * @since 2022-05-10
 */
public interface PreContractService extends IService<PreContractDO> {

}
