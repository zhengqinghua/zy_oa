package com.hnxxxy.oa.schedule.service;

import com.hnxxxy.oa.schedule.model.NoteDO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hnxxxy.oa.schedule.request.NoteRequest;
import com.hnxxxy.oa.schedule.request.ScheduleRequest;
import com.hnxxxy.oa.schedule.vo.NoteVo;
import com.hnxxxy.oa.schedule.vo.ScheduleVo;

import java.util.List;

/**
 * <p>
 * 个人便签表 服务类
 * </p>
 *
 * @author hzh
 * @since 2022-05-10
 */
public interface NoteService{
    /**
     * 创建便签
     */
    int insertNote(NoteRequest noteRequest);
    /**
     * 查询所有便签
     */
    List<NoteVo> findAll();
    /**
     * 删除便签
     */
    int deleteNote(int id);
    //更新便签
    int updateNote(int id, NoteRequest noteRequest);

}
