package com.hnxxxy.oa.schedule.mapper;

import com.hnxxxy.oa.schedule.model.ScheduleDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 日程表 Mapper 接口
 * </p>
 *
 * @author hzh
 * @since 2022-05-10
 */
public interface ScheduleMapper extends BaseMapper<ScheduleDO> {

}
