package com.hnxxxy.oa.schedule.mapper;

import com.hnxxxy.oa.schedule.model.PreContractDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 预约人表 Mapper 接口
 * </p>
 *
 * @author hzh
 * @since 2022-05-10
 */
public interface PreContractMapper extends BaseMapper<PreContractDO> {

}
