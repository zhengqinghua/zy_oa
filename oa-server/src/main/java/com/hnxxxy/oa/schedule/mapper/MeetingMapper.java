package com.hnxxxy.oa.schedule.mapper;

import com.hnxxxy.oa.schedule.model.MeetingDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会议类型表 Mapper 接口
 * </p>
 *
 * @author hzh
 * @since 2022-05-10
 */
public interface MeetingMapper extends BaseMapper<MeetingDO> {

}
