package com.hnxxxy.oa.schedule.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;

import java.time.LocalDateTime;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 会议类型表
 * </p>
 *
 * @author hzh
 * @since 2022-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("meeting")
public class MeetingDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 会议类型Id
     * 1=公司年会 2=机构会议 3=部门会议 4小组会议 5外部合作会议
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 会议类型名称
     */
    private String name;

    /**
     * 创建时间
     */
    private Date createTime;


}
