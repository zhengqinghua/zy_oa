package com.hnxxxy.oa.schedule.service;

import com.hnxxxy.oa.information.request.MessageRequest;
import com.hnxxxy.oa.schedule.model.ScheduleDO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hnxxxy.oa.schedule.request.SchedulePageRequest;
import com.hnxxxy.oa.schedule.request.ScheduleRequest;
import com.hnxxxy.oa.schedule.vo.ScheduleVo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 日程表 服务类
 * </p>
 *
 * @author hzh
 * @since 2022-05-10
 */
public interface ScheduleService {
    /**
     * 创建日程
     */
    int insertSchedule(ScheduleRequest scheduleRequest);
    /**
     * 查询所有日程
     */
    List<ScheduleVo> findAll();
    /**
     * 删除日程
     */
    int deleteSchedule(int id);
    //更新日程
    int updateSchedule(int id,ScheduleRequest scheduleRequest);
    /**
     * 分页查询消息
     * @return
     */
    Map<String, Object> page(SchedulePageRequest schedulePageRequest);

}
