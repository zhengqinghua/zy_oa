package com.hnxxxy.oa.schedule.service;

import com.hnxxxy.oa.information.request.MessageTypePageRequest;
import com.hnxxxy.oa.information.vo.MessageTypeVO;
import com.hnxxxy.oa.schedule.model.MeetingDO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hnxxxy.oa.schedule.request.MeetingPageRequest;
import com.hnxxxy.oa.schedule.request.MeetingRequest;
import com.hnxxxy.oa.schedule.vo.MeetingVo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 会议类型表 服务类
 * </p>
 *
 * @author hzh
 * @since 2022-05-10
 */
public interface MeetingService extends IService<MeetingDO> {

    void add(MeetingRequest meetingRequest);
    void delete(int id);
    List<MeetingVo> findAllMeetingType();
    Map<String, Object> page(MeetingPageRequest meetingPageRequest);
    MeetingVo findByIdMeetingType(int id);
}
