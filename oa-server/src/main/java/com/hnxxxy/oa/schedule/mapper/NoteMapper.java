package com.hnxxxy.oa.schedule.mapper;

import com.hnxxxy.oa.schedule.model.NoteDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 个人便签表 Mapper 接口
 * </p>
 *
 * @author hzh
 * @since 2022-05-10
 */
public interface NoteMapper extends BaseMapper<NoteDO> {

}
