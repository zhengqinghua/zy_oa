package com.hnxxxy.oa.schedule.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;


/**
 * @author hzh
 * @data 2022/5/22 19:33
 **/
@ApiModel(value = "SchedulePageRequest",description = "日程分页对象")
@Data
public class SchedulePageRequest {

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页",example = "1")
    private Integer page;

    /**
     * 每页条数
     */
    @ApiModelProperty(value = "每页显示多少条",example = "10")
    private Integer size;
    /**
     * 日程标题
     */
    @ApiModelProperty(value = "标题")
    private String title;

    /**
     * 会议地址
     */
    @ApiModelProperty(value = "会议地址")
    private String address;

    /**
     * 会议类型
     */
    @ApiModelProperty(value = "会议类型")
    @JsonProperty("meeting_id")
    private int meetingId;

    /**
     * 日程开始时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd",locale = "zh",timezone = "GMT+8")
    @JsonProperty("begin_time")
    private Date beginTime;

    /**
     * 日程结束时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd",locale = "zh",timezone = "GMT+8")
    @JsonProperty("end_time")
    private Date endTime;

    /**
     * 日程内容
     */
    @ApiModelProperty(value = "日程内容")
    private String content;

    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @JsonProperty("create_by")
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss",locale = "zh",timezone = "GMT+8")
    @JsonProperty("create_time")
    private Date createTime;

    /**
     * 是否私有
     */
    @ApiModelProperty(value = "是否私有")
    @JsonProperty("is_private")
    private int isPrivate;
}
