package com.hnxxxy.oa.schedule.request;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author hzh
 * @data 2022/5/22 15:30
 **/
@ApiModel(value = "消息类型请求",description = "消息类型请求对象")
@Data
public class MeetingRequest {
    /**
     * 会议类型Id
     * 1=公司年会 2=机构会议 3=部门会议 4小组会议 5外部合作会议
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 会议类型名称
     */
    @ApiModelProperty(value = "会议类型名称",example = "公司年会")
    private String name;

    /**
     * 创建时间
     */
    private Date createTime;
}
