package com.hnxxxy.oa.schedule.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;

import java.time.LocalDateTime;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 预约人表
 * </p>
 *
 * @author hzh
 * @since 2022-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("pre_contract")
public class PreContractDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 预约序号Id
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 日程Id
     */
    private Integer scheduleId;

    /**
     * 预约人
     */
    private Integer userId;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;


}
