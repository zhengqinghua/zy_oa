package com.hnxxxy.oa.schedule.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import java.util.Date;

/**
 * @author hzh
 * @data 2022/5/12 19:44
 **/
@Data
public class ScheduleVo {

    private static final long serialVersionUID = 1L;

    /**
     * 日程Id
     */
    private Integer id;

    /**
     * 日程标题
     */
    private String title;

    /**
     * 会议地址
     */
    private String address;

    /**
     * 会议类型
     */
    private int meetingId;

    /**
     * 日程开始时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd",locale = "zh",timezone = "GMT+8")
    @JsonProperty("begin_time")
    private Date beginTime;

    /**
     * 日程结束时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd",locale = "zh",timezone = "GMT+8")
    @JsonProperty("end_time")
    private Date endTime;

    /**
     * 日程内容
     */
    private String content;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 是否私有
     */
    private Integer isPrivate;

}
