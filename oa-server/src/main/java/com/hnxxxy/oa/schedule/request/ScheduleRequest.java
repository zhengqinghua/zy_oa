package com.hnxxxy.oa.schedule.request;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author hzh
 * @data 2022/5/12 18:23
 **/
@Data
@ApiModel(value = "ScheduleRequest",description = "日程对象")
public class ScheduleRequest{
    /**
     * 日程标题
     */
    @ApiModelProperty(value = "标题")
    private String title;

    /**
     * 会议地址
     */
    @ApiModelProperty(value = "会议地址")
    private String address;

    /**
     * 会议类型
     */
    @ApiModelProperty(value = "会议类型")
    @JsonProperty("meeting_id")
    private int meetingId;

    /**
     * 日程开始时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd",locale = "zh",timezone = "GMT+8")
    @JsonProperty("begin_time")
    private Date beginTime;

    /**
     * 日程结束时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd",locale = "zh",timezone = "GMT+8")
    @JsonProperty("end_time")
    private Date endTime;

    /**
     * 日程内容
     */
    @ApiModelProperty(value = "日程内容")
    private String content;

    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @JsonProperty("create_by")
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd",locale = "zh",timezone = "GMT+8")
    @JsonProperty("create_time")
    private Date createTime;

    /**
     * 是否私有
     */
    @ApiModelProperty(value = "是否私有")
    @JsonProperty("is_private")
    private int isPrivate;

}