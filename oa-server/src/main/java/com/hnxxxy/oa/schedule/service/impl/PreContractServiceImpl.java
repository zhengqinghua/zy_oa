package com.hnxxxy.oa.schedule.service.impl;

import com.hnxxxy.oa.schedule.model.PreContractDO;
import com.hnxxxy.oa.schedule.mapper.PreContractMapper;
import com.hnxxxy.oa.schedule.service.PreContractService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 预约人表 服务实现类
 * </p>
 *
 * @author hzh
 * @since 2022-05-10
 */
@Service
public class PreContractServiceImpl extends ServiceImpl<PreContractMapper, PreContractDO> implements PreContractService {

}
