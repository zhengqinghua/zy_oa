package com.hnxxxy.oa.schedule.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hnxxxy.oa.information.model.MessageDO;
import com.hnxxxy.oa.information.request.MessageRequest;
import com.hnxxxy.oa.information.vo.MessageVO;
import com.hnxxxy.oa.schedule.model.ScheduleDO;
import com.hnxxxy.oa.schedule.mapper.ScheduleMapper;
import com.hnxxxy.oa.schedule.request.SchedulePageRequest;
import com.hnxxxy.oa.schedule.request.ScheduleRequest;
import com.hnxxxy.oa.schedule.service.ScheduleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hnxxxy.oa.schedule.vo.ScheduleVo;
import com.hnxxxy.oa.system.constant.UserConstant;
import com.hnxxxy.oa.system.model.SysUserDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 日程表 服务实现类
 * </p>
 *
 * @author hzh
 * @since 2022-05-10
 */
@Slf4j
@Service
public class ScheduleServiceImpl extends ServiceImpl<ScheduleMapper, ScheduleDO> implements ScheduleService {

    @Autowired
    private ScheduleMapper scheduleMapper;
    @Override
    public int insertSchedule(ScheduleRequest scheduleRequest) {
        SysUserDO user = (SysUserDO) StpUtil.getSession().get(UserConstant.SESSION_USER_KEY);
        ScheduleDO scheduleDO = new ScheduleDO();
        log.info(scheduleRequest.toString());
        BeanUtils.copyProperties(scheduleRequest,scheduleDO);
        scheduleDO.setCreateTime( new Date());
        scheduleDO.setCreateBy(user.getActualName());
        log.info(scheduleDO.toString());
        int rows = scheduleMapper.insert(scheduleDO);
        return rows;
    }

    @Override
    public List<ScheduleVo> findAll() {
        SysUserDO user = (SysUserDO) StpUtil.getSession().get(UserConstant.SESSION_USER_KEY);
        List<ScheduleDO> scheduleDOList = scheduleMapper.selectList(new QueryWrapper<ScheduleDO>().eq("id", user.getId()));

        List<ScheduleVo> scheduleVOList = scheduleDOList.stream().map(obj -> {
            ScheduleVo scheduleVO = new ScheduleVo();
            BeanUtils.copyProperties(obj, scheduleVO);
            return scheduleVO;
        }).collect(Collectors.toList());

        log.info("全部消息:scheduleVOList={}",scheduleVOList);
        return scheduleVOList;
    }

    @Override
    public int deleteSchedule(int id) {
        SysUserDO user = (SysUserDO) StpUtil.getSession().get("userDO");
        int rows = scheduleMapper.delete(new QueryWrapper<ScheduleDO>().eq("id", id));
        log.info("删除消息:rows={}",rows);
        return rows;
    }

    @Override
    public Map<String, Object> page(SchedulePageRequest schedulePageRequest) {
        int page = schedulePageRequest.getPage();
        int size = schedulePageRequest.getSize();
        SysUserDO user = (SysUserDO) StpUtil.getSession().get(UserConstant.SESSION_USER_KEY);
        Date beginTime = schedulePageRequest.getBeginTime();
        Date endTime = schedulePageRequest.getEndTime();
        int isPrivate = schedulePageRequest.getIsPrivate();
        String title = schedulePageRequest.getTitle();


        QueryWrapper<ScheduleDO> queryWrapper = new QueryWrapper<ScheduleDO>();

        if (Objects.nonNull(beginTime)) {
            queryWrapper.ge("begin_time", beginTime);
        }

        if (Objects.nonNull(endTime)) {
            queryWrapper.le("end_time", endTime);
        }

        if (isPrivate != 0){
            queryWrapper.eq("is_private", isPrivate);
        }

        if (title != null){
            queryWrapper.like("title", title);
        }


        Page<ScheduleDO> pageInfo = new Page<>(page, size);

        IPage<ScheduleDO> scheduleDOIPage = scheduleMapper.selectPage(pageInfo,queryWrapper);

        Map<String,Object> pageMap = new HashMap<>(3);

        pageMap.put("total_record", scheduleDOIPage.getTotal());
        pageMap.put("total_page", scheduleDOIPage.getPages());
        pageMap.put("current_data",scheduleDOIPage.getRecords()
                .stream().map(obj->beanProocess(obj)).collect(Collectors.toList()));

        log.info("分页消息:pageMap={}",pageMap);
        return pageMap;
    }

    @Override
    public int updateSchedule(int id,ScheduleRequest scheduleRequest) {
        SysUserDO user = (SysUserDO) StpUtil.getSession().get(UserConstant.SESSION_USER_KEY);
        ScheduleDO scheduleDO = new ScheduleDO();
        BeanUtils.copyProperties(scheduleRequest, scheduleDO);
        int rows = scheduleMapper.update(scheduleDO, new QueryWrapper<ScheduleDO>().eq("id", id));
        log.info("修改消息:rows={}",rows);
        return rows;
    }
    private ScheduleVo beanProocess(ScheduleDO obj) {
        ScheduleVo scheduleVo = new ScheduleVo();
        BeanUtils.copyProperties(obj, scheduleVo);
        return scheduleVo;
    }
}
