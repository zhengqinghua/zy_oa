package com.hnxxxy.oa.schedule.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.hnxxxy.oa.schedule.model.MeetingDO;
import com.hnxxxy.oa.schedule.mapper.MeetingMapper;
import com.hnxxxy.oa.schedule.request.MeetingPageRequest;
import com.hnxxxy.oa.schedule.request.MeetingRequest;
import com.hnxxxy.oa.schedule.service.MeetingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hnxxxy.oa.schedule.vo.MeetingVo;
import com.hnxxxy.oa.system.constant.UserConstant;
import com.hnxxxy.oa.system.model.SysUserDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 会议类型表 服务实现类
 * </p>
 *
 * @author hzh
 * @since 2022-05-10
 */
@Slf4j
@Service
public class MeetingServiceImpl extends ServiceImpl<MeetingMapper, MeetingDO> implements MeetingService {
    @Autowired
    private MeetingMapper meetingMapper;

    @Override
    public void add(MeetingRequest meetingRequest) {
        MeetingDO meetingDO = new MeetingDO();
        meetingDO.setCreateTime(new Date());
        System.out.println("===================="+meetingDO);
        BeanUtils.copyProperties(meetingRequest,meetingDO);
        int rows = meetingMapper.insert(meetingDO);
        log.info("{},{}",rows,meetingDO);
    }

    @Override
    public void delete(int id) {
        int rows = meetingMapper.delete(new QueryWrapper<MeetingDO>().eq("id",id));
        log.info("删除{}行",rows);
    }

    @Override
    public List<MeetingVo> findAllMeetingType() {
        List<MeetingDO> meetingDOList = meetingMapper.selectList(null);

        List<MeetingVo> meetingVOList = meetingDOList.stream().map(obj -> {
            MeetingVo meetingVo = new MeetingVo();
            BeanUtils.copyProperties(obj, meetingVo);
            return meetingVo;
        }).collect(Collectors.toList());

        log.info("全部会议类型:meetingVOList={}",meetingDOList);
        return meetingVOList;
    }

    @Override
    public Map<String, Object> page(MeetingPageRequest meetingPageRequest) {

        int page = meetingPageRequest.getPage();
        int size = meetingPageRequest.getSize();
        SysUserDO user = (SysUserDO) StpUtil.getSession().get(UserConstant.SESSION_USER_KEY);
        String name = meetingPageRequest.getName();
        QueryWrapper<MeetingDO> queryWrapper = new QueryWrapper<MeetingDO>();
        if(!StrUtil.hasEmpty(name)){
            queryWrapper.like("name", name);
        }

        Page<MeetingDO> pageInfo = new Page<>(page, size);

        IPage<MeetingDO> meetingDOIPage = meetingMapper.selectPage(pageInfo,queryWrapper);

        Map<String,Object> pageMap = new HashMap<>(3);

        pageMap.put("total_record", meetingDOIPage.getTotal());
        pageMap.put("total_page", meetingDOIPage.getPages());
        pageMap.put("current_data",meetingDOIPage.getRecords()
                .stream().map(obj->beanProocess(obj)).collect(Collectors.toList()));

        log.info("分页消息:pageMap={}",pageMap);
        return pageMap;
    }
    private MeetingVo beanProocess(MeetingDO obj) {
        MeetingVo meetingVO = new MeetingVo();
        BeanUtils.copyProperties(obj, meetingVO);
        return meetingVO;
    }

    @Override
    public MeetingVo findByIdMeetingType(int id) {
        return null;
    }
}
