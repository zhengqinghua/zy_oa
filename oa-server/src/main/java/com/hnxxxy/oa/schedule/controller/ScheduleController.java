package com.hnxxxy.oa.schedule.controller;


import com.hnxxxy.oa.information.request.MessageRequest;
import com.hnxxxy.oa.schedule.model.ScheduleDO;
import com.hnxxxy.oa.schedule.request.SchedulePageRequest;
import com.hnxxxy.oa.schedule.request.ScheduleRequest;
import com.hnxxxy.oa.schedule.service.MeetingService;
import com.hnxxxy.oa.schedule.service.ScheduleService;
import com.hnxxxy.oa.schedule.vo.MeetingVo;
import com.hnxxxy.oa.schedule.vo.ScheduleVo;
import com.hnxxxy.oa.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 日程表 前端控制器
 * </p>
 *
 * @author hzh
 * @since 2022-05-10
 */
@Api(value = "scheduleController",tags = "日程接口")
@RestController
@RequestMapping("/api/schedule/v1")
public class ScheduleController {
    @Autowired
    ScheduleService scheduleService;
    @Autowired
    MeetingService meetingService;

    @PostMapping("/add")
    @ApiOperation(value = "创建日程")
    public R Add(@ApiParam("日程对象") @RequestBody ScheduleRequest scheduleRequest){
        scheduleService.insertSchedule(scheduleRequest);
        return R.buildSuccess("创建日程成功");
    }
    @PostMapping("/meetinglist")
    @ApiOperation(value = "查询全部会议类型")
    public R findAllMeeting(){
        List<MeetingVo> meetingVo = meetingService.findAllMeetingType();
        return R.buildSuccess(meetingVo);
    }
    @PostMapping("/list")
    @ApiOperation(value = "查询全部行程")
    public R FindAllSchedules(){
        scheduleService.findAll();
        return R.buildSuccess();
    }
    @GetMapping("/del/{id}")
    @ApiModelProperty(value = "删除日程")
    public R delete(@ApiParam(value = "消息id",required = true)
                    @PathVariable("id") int id){
        scheduleService.deleteSchedule(id);
        return R.buildSuccess("删除日程成功");
    }
    @GetMapping("/update/{id}")
    @ApiOperation(value = "修改日程")
    public R update(@ApiParam(value = "日程id",required = true)
                    @PathVariable("id") int id,
                    @ApiParam("日程对象")
                    @RequestBody ScheduleRequest scheduleRequest){
        scheduleService.updateSchedule(id,scheduleRequest);
        return R.buildSuccess("修改日程成功");
    }
    @ApiOperation("分页查询消息列表")
    @PostMapping("/page")
    public R pageMeetingList(
            @ApiParam("日程分页对象") @RequestBody
                    SchedulePageRequest schedulePageRequest
    ){
        Map<String,Object> pageResult = scheduleService.page(schedulePageRequest);
        return R.buildSuccess(pageResult);
    }
}

