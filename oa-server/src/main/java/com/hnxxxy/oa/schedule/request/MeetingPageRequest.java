package com.hnxxxy.oa.schedule.request;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author hzh
 * @data 2022/5/22 18:59
 **/
@ApiModel(value = "会议请求",description = "会议请求对象")
@Data
public class MeetingPageRequest {
    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页",example = "1")
    private Integer page;

    /**
     * 每页条数
     */
    @ApiModelProperty(value = "每页显示多少条",example = "10")
    private Integer size;
    /**
     * 会议类型Id
     * 1=公司年会 2=机构会议 3=部门会议 4小组会议 5外部合作会议
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 会议类型名称
     */
    @ApiModelProperty(value = "会议类型名称",example = "公司年会")
    private String name;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;
}
