package com.hnxxxy.oa.schedule.controller;


import com.hnxxxy.oa.schedule.request.NoteRequest;
import com.hnxxxy.oa.schedule.request.ScheduleRequest;
import com.hnxxxy.oa.schedule.service.NoteService;
import com.hnxxxy.oa.schedule.service.ScheduleService;
import com.hnxxxy.oa.utils.R;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 个人便签表 前端控制器
 * </p>
 *
 * @author hzh
 * @since 2022-05-10
 */
@RestController
@RequestMapping("/noteDO")
public class NoteController {
    @Autowired
    NoteService noteService;

    @PostMapping("/add")
    @ApiOperation(value = "创建便条")
    public R Add(@ApiParam("便条对象") @RequestBody NoteRequest noteRequest){
        noteService.insertNote(noteRequest);
        return R.buildSuccess("创建便条成功");
    }
    @PostMapping("/list")
    @ApiOperation(value = "查询全部便条")
    public R FindAllNotes(){
        noteService.findAll();
        return R.buildSuccess();
    }
    @GetMapping("/delete/{id}")
    @ApiModelProperty(value = "删除便条")
    public R delete(@ApiParam(value = "便条id",required = true)
                    @PathVariable("id") int id){
        noteService.deleteNote(id);
        return R.buildSuccess("删除便条成功");
    }
    @GetMapping("/update/{id}")
    @ApiOperation(value = "便条消息")
    public R update(@ApiParam(value = "便条id",required = true)
                    @PathVariable("id") int id,
                    @ApiParam("便条对象")
                    @RequestBody NoteRequest noteRequest){
        noteService.updateNote(id,noteRequest);
        return R.buildSuccess("修改便条成功");
    }
}

