package com.hnxxxy.oa.schedule.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;

/**
 * @author hzh
 * @data 2022/5/12 22:00
 **/
public class NoteRequest {
    /**
     * 便签id
     */
    @ApiModelProperty(value = "便签id")
    private Integer id;

    /**
     * 便签标题
     */
    @ApiModelProperty(value = "标题")
    private String title;

    /**
     * 便签内容
     */
    @ApiModelProperty(value = "内容")
    private String content;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss",locale = "zh",timezone = "GMT+8")
    @JsonProperty("create_time")
    private LocalDateTime createTime;

    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    private String createBy;
}
