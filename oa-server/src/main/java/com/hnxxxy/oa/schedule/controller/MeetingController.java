package com.hnxxxy.oa.schedule.controller;


import com.hnxxxy.oa.information.request.MessageRequest;
import com.hnxxxy.oa.information.request.MessageTypePageRequest;
import com.hnxxxy.oa.information.vo.MessageVO;
import com.hnxxxy.oa.schedule.request.MeetingPageRequest;
import com.hnxxxy.oa.schedule.request.MeetingRequest;
import com.hnxxxy.oa.schedule.request.ScheduleRequest;
import com.hnxxxy.oa.schedule.service.MeetingService;
import com.hnxxxy.oa.schedule.vo.MeetingVo;
import com.hnxxxy.oa.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 会议类型表 前端控制器
 * </p>
 *
 * @author hzh
 * @since 2022-05-10
 */
@RestController
@RequestMapping("/api/meeting/v1")
@Api(value = "MeetingController",tags={"会议类型"})
public class MeetingController {
    @Autowired
    private MeetingService meetingService;
    @PostMapping("/add")
    @ApiOperation(value = "创建会议")
    public R add(@ApiParam("会议类型对象") @RequestBody MeetingRequest meetingRequest){
        meetingService.add(meetingRequest);
        return R.buildSuccess("新增会议类型");
    }

    @ApiOperation(value = "删除会议类型")
    @GetMapping("/del/{id}")
    public R delete(@ApiParam(value = "消息id",required = true)
                    @PathVariable("id") int id){
        meetingService.delete(id);
        return R.buildSuccess("删除会议类型成功");
    }
    @PostMapping("/list")
    @ApiOperation(value = "查询全部会议类型")
    public R findAllMeeting(){
        List<MeetingVo> meetingVo = meetingService.findAllMeetingType();
        return R.buildSuccess(meetingVo);
    }
    @ApiOperation("分页查询消息列表")
    @PostMapping("/page")
    public R pageMeetingList(

            @ApiParam("会议类型分页对象") @RequestBody
                    MeetingPageRequest meetingPageRequest
    ){
        Map<String,Object> pageResult = meetingService.page(meetingPageRequest);
        return R.buildSuccess(pageResult);
    }
}

