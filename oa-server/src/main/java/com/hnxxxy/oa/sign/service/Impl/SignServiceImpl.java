package com.hnxxxy.oa.sign.service.Impl;

import com.hnxxxy.oa.sign.VOfactorys.Impl.SignInfoFactory;
import com.hnxxxy.oa.sign.VOfactorys.Impl.StatisticDataFactory;
import com.hnxxxy.oa.sign.entity.SignDataVO;
import com.hnxxxy.oa.sign.entity.SignInfoVO;
import com.hnxxxy.oa.sign.entity.StatisticVO;
import com.hnxxxy.oa.sign.mapper.SignMapper;
import com.hnxxxy.oa.sign.request.SearchDataRequest;
import com.hnxxxy.oa.sign.request.SearchStatisticRequest;
import com.hnxxxy.oa.sign.request.SignCheckRequest;
import com.hnxxxy.oa.sign.request.SignRequest;
import com.hnxxxy.oa.sign.service.SignService;
import com.hnxxxy.oa.sign.util.SignParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * @author 1112
 * 2022/5/8
 */
@Service
public class SignServiceImpl implements SignService {

    private final SignMapper signMapper;
    private final SignParser signParser;
    private final SignInfoFactory signInfoFactory;
    private final StatisticDataFactory statisticDataFactory;

    @Autowired
    public SignServiceImpl(SignMapper signMapper, SignParser parser, SignInfoFactory factory, StatisticDataFactory dataFactory) {
        this.signMapper = signMapper;
        signParser = parser;
        signInfoFactory = factory;
        statisticDataFactory = dataFactory;
    }

    @Override
    public int signIn(SignRequest request) throws ParseException {
        LocalDateTime signTime = request.getTime();
        // LocalDate转换为Date对象
        Date signDate = SimpleDateFormat.getDateInstance().parse(signTime.toLocalDate().toString());
        //进行签到操作之前对请求目标的签到状态进行检查
        if (checkSign(new SignCheckRequest(request.getUserId(), signDate)) != 0) {
            // 如果当前用户当天已签到/已签退,停止操作并返回对应状态
            return -1;
        }
        // 向数据库插入签到数据
        signMapper.insert(signParser.parse(request));
        //插入登录数据成功返回对应状态
        return 1;
    }

    @Override
    public int signOut(SignRequest request) throws ParseException {
        LocalDateTime signTime = request.getTime();
        // LocalDate转换为Date对象
        Date signDate = SimpleDateFormat.getDateInstance().parse(signTime.toLocalDate().toString());
        //进行签退操作之前对请求目标的签到状态进行检查
        int status = checkSign(new SignCheckRequest(request.getUserId(), signDate));
        if (status == 0) {
            //当用户未进行签到,停止操作返回对应状态
            return 0;
        }
        if (status == 2) {
            //当前用户已完成了签退,停止操作返回对应状态
            return -1;
        }
        // 向数据库插入签退数据
        signMapper.insert(signParser.parse(request));
        // 签退退操作成功返回对应操作
        return 1;
    }

    @Override
    public int checkSign(SignCheckRequest request) {
        return signMapper.checkSignById(request);
    }

    @Override
    public List<SignDataVO> searchSignData(SearchDataRequest searchDataRequest) {
        return signInfoFactory.buildBatch(searchDataRequest);
    }

    @Override
    public StatisticVO searchStatisticData(SearchStatisticRequest searchStatisticRequest) {
        return statisticDataFactory.build(searchStatisticRequest);
    }

    @Override
    public SignInfoVO getSignInfo(SignRequest request) {
        return signInfoFactory.build(request);
    }
}
