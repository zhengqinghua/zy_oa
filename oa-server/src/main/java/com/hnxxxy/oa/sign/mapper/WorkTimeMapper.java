package com.hnxxxy.oa.sign.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hnxxxy.oa.sign.entity.WorktimeDO;
import org.springframework.stereotype.Repository;

/**
 * @author 1112
 * 2022/5/8
 */
@Repository
public interface WorkTimeMapper extends BaseMapper<WorktimeDO> {

}
