package com.hnxxxy.oa.sign.request;

import lombok.Data;

import java.util.Date;

/**
 * @author 1112
 * 2022/5/12
 */
@Data
public class SearchStatisticRequest {
    /**
     * 机构Id
     */
    private Integer mechanismId;
    /**
     * 部门Id
     */
    private Integer departmentId;
    /**
     * 查询开始时间
     */
    private Date startTime;
    /**
     * 查询结束时间
     */
    private Date endTime;
    /**
     * 请求用户姓名
     */
    private String requestUserName;
    /**
     * 请求时间
     */
    private Date requestTime;

    public SearchStatisticRequest(Integer mechanismId, Integer departmentId, Date startTime, Date endTime, String requestUserName, Date requestTime) {
        this.mechanismId = mechanismId;
        this.departmentId = departmentId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.requestUserName = requestUserName;
        this.requestTime = requestTime;
    }

    public SearchStatisticRequest() {
    }
}
