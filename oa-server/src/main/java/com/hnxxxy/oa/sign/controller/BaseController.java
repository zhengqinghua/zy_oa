package com.hnxxxy.oa.sign.controller;

import cn.dev33.satoken.session.SaSession;
import cn.dev33.satoken.stp.StpUtil;
import com.hnxxxy.oa.system.constant.UserConstant;
import com.hnxxxy.oa.system.model.SysUserDO;

/**
 * @author 1112
 * 2022/5/21
 */
abstract class BaseController {
    /**
     * @return 当前session中的用户信息
     * 此方法用于获得session中的用户数据
     */
    SysUserDO getCurrUserData() {
        SaSession session = StpUtil.getSession();
        return (SysUserDO) session.get(UserConstant.SESSION_USER_KEY);
    }
}
