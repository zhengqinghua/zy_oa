package com.hnxxxy.oa.sign.entity;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author 1112
 * 2022/5/11
 * 此对象作为SignIn/SignOut操作成功返回的视图对象
 */
@Data
public class SignInfoVO {
    /**
     * 用户名
     */
    private String username;
    /**
     * 用户真实姓名
     */
    private String actualName;
    /**
     * 部门名称
     */
    private String departmentName;
    /**
     * 机构名称
     */
    private String mechanismName;
    /**
     * 签到/签退时间
     */
    private LocalDateTime signTime;
    /**
     * 签到/签退备注
     */
    private String signInfo;

}
