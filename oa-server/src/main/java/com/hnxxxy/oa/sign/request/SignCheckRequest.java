package com.hnxxxy.oa.sign.request;

import lombok.Data;

import java.util.Date;

/**
 * @author 1112
 * 2022/5/12
 */
@Data
public class SignCheckRequest {
    /**
     * 用户Id
     */
    private Integer userId;
    /**
     * 将查询的时间
     */
    private Date queryTime;

    public SignCheckRequest(Integer userId, Date queryTime) {
        this.userId = userId;
        this.queryTime = queryTime;
    }
}
