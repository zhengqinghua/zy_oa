package com.hnxxxy.oa.sign.VOfactorys;

/**
 * @author 1112
 * 2022/5/11
 */
public interface VOFactory<T, P> {
    /**
     * @param data 创建所需要的数据
     * @return 指定VO对象
     * <p>
     * 通过指定输入自动创建组装VO对象
     */
    T build(P data);
}
