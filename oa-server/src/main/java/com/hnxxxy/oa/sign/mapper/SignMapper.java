package com.hnxxxy.oa.sign.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hnxxxy.oa.sign.entity.SignDO;
import com.hnxxxy.oa.sign.entity.SignDataVO;
import com.hnxxxy.oa.sign.entity.UserDeptDTO;
import com.hnxxxy.oa.sign.entity.UserStatisticDTO;
import com.hnxxxy.oa.sign.request.SearchDataRequest;
import com.hnxxxy.oa.sign.request.SearchStatisticRequest;
import com.hnxxxy.oa.sign.request.SignCheckRequest;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 1112
 * 2022/5/8
 */
@Repository
public interface SignMapper extends BaseMapper<SignDO> {
    /**
     * @param userId 用户Id
     * @return 对应用户的部门机构信息
     * 查询对应用户的部门与机构名称
     */
    UserDeptDTO findUserDept(Integer userId);

    /**
     * @param range 包含统计范围的请求对象
     * @return 对应范围的的用户统计信息
     */
    List<UserStatisticDTO> findSignByTime(SearchStatisticRequest range);

    /**
     * @param request 包含信息的请求对象
     * @return 返回指定日期的目标用户的签到状态
     */
    Integer checkSignById(SignCheckRequest request);

    /**
     * @param request 包含所需信息的请求对象
     * @return 返回对应的用户登录数据
     */
    List<SignDataVO> searchSignData(SearchDataRequest request);
}
