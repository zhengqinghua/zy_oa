package com.hnxxxy.oa.sign.VOfactorys.Impl;

import com.hnxxxy.oa.sign.VOfactorys.VOFactory;
import com.hnxxxy.oa.sign.entity.StatisticVO;
import com.hnxxxy.oa.sign.entity.UserStatisticDTO;
import com.hnxxxy.oa.sign.mapper.SignMapper;
import com.hnxxxy.oa.sign.request.SearchStatisticRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 1112
 * 2022/5/11
 */

@Component
public class StatisticDataFactory implements VOFactory<StatisticVO, SearchStatisticRequest> {
    private final SignMapper mapper;

    @Autowired
    public StatisticDataFactory(SignMapper signMapper) {
        mapper = signMapper;
    }

    @Override
    public StatisticVO build(SearchStatisticRequest statisticRequest) {
        StatisticVO statisticVO = new StatisticVO();
        ArrayList<UserStatisticDTO> statisticList = new ArrayList<>();

        // 从数据库中得到统计数据
        List<UserStatisticDTO> datalist = mapper.findSignByTime(statisticRequest);

        // 计算迟到率
        for (UserStatisticDTO i : datalist) {
            i.setAttendanceRate((i.getAbsenteeismTimes() * 100 * 100) / i.getWorkDays());
            statisticList.add(i);
        }

        // 设置对象中的属性
        statisticVO.setDataList(statisticList);
        statisticVO.setRequestTime(statisticRequest.getRequestTime());
        statisticVO.setRequestUserName(statisticRequest.getRequestUserName());

        return statisticVO;
    }
}
