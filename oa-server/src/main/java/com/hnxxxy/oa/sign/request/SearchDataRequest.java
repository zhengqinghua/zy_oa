package com.hnxxxy.oa.sign.request;

import lombok.Data;

import java.util.Date;

/**
 * @author 1112
 * 2022/5/12
 */
@Data
public class SearchDataRequest {
    /**
     * 查询开始时间
     */
    private Date startTime;
    /**
     * 查询结束时间
     */
    private Date endTime;
    /**
     * 机构Id
     */
    private Integer mechanismId;
    /**
     * 部门Id
     */
    private Integer departmentId;
    /**
     * 员工Id
     */
    private Integer userId;
    /**
     * 员工真实姓名
     */
    private String actualName;

    public SearchDataRequest() {
    }

    public SearchDataRequest(Date startTime, Date endTime, Integer mechanismId, Integer departmentId, Integer userId, String actualName) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.mechanismId = mechanismId;
        this.departmentId = departmentId;
        this.userId = userId;
        this.actualName = actualName;
    }
}
