package com.hnxxxy.oa.sign.service;

import com.hnxxxy.oa.sign.entity.SignDataVO;
import com.hnxxxy.oa.sign.entity.SignInfoVO;
import com.hnxxxy.oa.sign.entity.StatisticVO;
import com.hnxxxy.oa.sign.request.SearchDataRequest;
import com.hnxxxy.oa.sign.request.SearchStatisticRequest;
import com.hnxxxy.oa.sign.request.SignCheckRequest;
import com.hnxxxy.oa.sign.request.SignRequest;

import java.text.ParseException;
import java.util.List;

/**
 * @author 1112
 * 2022/5/8
 */
public interface SignService {
    /**
     * @param request 签到请求信息
     *                进行执行系统签到操作
     * @return 1:签到成功
     * -1:重复签到
     * @throws ParseException 请求信息不完整或错误造成转换异常
     */
    int signIn(SignRequest request) throws ParseException;

    /**
     * @param request 签退请求信息
     *                执行系统签退工作
     * @return 1:签退成功
     * 0:未签到
     * -1:重复签退
     */
    int signOut(SignRequest request) throws ParseException;

    /**
     * 查询签到状态
     *
     * @param request 所查询的信息
     * @return 签到状态
     * 0: 未签到
     * 1: 已签到未签退
     * 2: 已签退
     */
    int checkSign(SignCheckRequest request);

    /**
     * @param searchDataRequest 搜索请求信息
     * @return 所查找的登录数据
     */
    List<SignDataVO> searchSignData(SearchDataRequest searchDataRequest);

    /**
     * @param searchStatisticRequest 统计请求信息
     * @return 请求的数据统计信息
     * 在数据库中查找并统计信息
     */
    StatisticVO searchStatisticData(SearchStatisticRequest searchStatisticRequest);

    /**
     * @param request 签到/签出数据
     * @return StatisticVO类型的签到/签出相关视图信息
     * 获取签到/签出相关视图信息
     */
    SignInfoVO getSignInfo(SignRequest request);
}
