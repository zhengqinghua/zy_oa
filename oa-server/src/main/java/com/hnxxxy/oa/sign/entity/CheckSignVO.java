package com.hnxxxy.oa.sign.entity;

import com.hnxxxy.oa.system.model.SysUserDO;
import lombok.Data;

/**
 * @author 1112
 * 2022/5/25
 * 本对象作为CheckSign返回的视图对象使用
 */
@Data
public class CheckSignVO {

    /**
     * 当前会话用户
     */
    private SysUserDO currUser;
    /**
     * 当前登录的状态
     */
    private Integer status;
}
