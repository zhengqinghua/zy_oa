package com.hnxxxy.oa.sign.controller;

import com.hnxxxy.oa.sign.entity.SignDataVO;
import com.hnxxxy.oa.sign.entity.StatisticVO;
import com.hnxxxy.oa.sign.request.SearchDataRequest;
import com.hnxxxy.oa.sign.request.SearchStatisticRequest;
import com.hnxxxy.oa.sign.service.SignService;
import com.hnxxxy.oa.system.model.SysUserDO;
import com.hnxxxy.oa.utils.R;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author 1112
 * 2022/5/13
 */
@RestController
@RequestMapping("/apis")
@Api(value = "signDataController", tags = {"考勤数据模块"})
public class SignDataController extends BaseController {
    private final SignService service;
    private final Logger logger;

    @Autowired
    public SignDataController(SignService service) {
        this.service = service;
        this.logger = LoggerFactory.getLogger(SignDataController.class);
    }

    @RequestMapping(value = "/sign/signData", method = RequestMethod.POST)
    public R signData(@Nullable @RequestParam String startTime,
                      @Nullable @RequestParam String endTime,
                      @Nullable @RequestParam Integer mechanismId,
                      @Nullable @RequestParam Integer departmentId,
                      @Nullable @RequestParam Integer userId,
                      @Nullable @RequestParam("actualName") String username) {
        //转换时间
        DateFormat dateFormat = SimpleDateFormat.getDateInstance();
        Date start = null;
        Date end = null;
        if (startTime != null) {
            try {
                start = dateFormat.parse(startTime);
                end = dateFormat.parse(endTime);
            } catch (ParseException e) {
                logger.error("时间转换错误" + e.getMessage());
                return R.buildError("时间格式有误");
            }
        }

        SearchDataRequest request = new SearchDataRequest(start, end, mechanismId, departmentId, userId, username);
        //记录参数
        logger.info("<---" + request);

        List<SignDataVO> list = null;
        try {
            // 获取数据
            list = service.searchSignData(request);
        } catch (Exception e) {
            logger.error("API内部异常\n" + e.getMessage());
        }
        return R.buildSuccess(list);
    }

    @RequestMapping(value = "/sign/signStatisticData", method = RequestMethod.POST)
    public R statisticData(@Nullable @RequestParam Integer mechanismId,
                           @Nullable @RequestParam Integer departmentId,
                           @Nullable @RequestParam String startTime,
                           @Nullable @RequestParam String endTime,
                           @Nullable @RequestParam String requestTime) {
        //获取 DateFormat 对象
        DateFormat dateFormat = SimpleDateFormat.getDateInstance();
        //获取签到用户信息
        SysUserDO userData = getCurrUserData();
        Date start;
        Date end;
        Date reqTime;
        try {
            // 将请求中的时间字符串转换为对应的对象
            start = dateFormat.parse(startTime);
            end = dateFormat.parse(endTime);
            reqTime = dateFormat.parse(requestTime);
        } catch (ParseException e) {
            logger.error("时间转换错误" + e.getMessage());
            return R.buildError("时间格式有误");
        }
        // 检查请求参数中的时间是否合理
        if (end.compareTo(start) <= 0) {
            logger.warn("时间间隔有误(最少间隔一天)start: " + startTime + "end: " + endTime);
            return R.buildError("时间间隔有误(最少间隔一天)");
        }
        //构建统计请求对象
        SearchStatisticRequest request = new SearchStatisticRequest(mechanismId, departmentId, start, end, userData.getActualName(), reqTime);
        //记录参数
        logger.info("<---" + request);
        StatisticVO statisticData;
        try {
            // 根据请求信息获取统计数据
            statisticData = service.searchStatisticData(request);
        } catch (Exception e) {
            logger.error("API内部错误\n" + e.getMessage());
            return R.buildError("查询失败");
        }
        return R.buildSuccess(statisticData);
    }
}
