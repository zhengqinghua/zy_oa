package com.hnxxxy.oa.sign.request;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author 1112
 * 2022/5/12
 */

@Data
public class SignRequest {
    /**
     * 签到/签退用户ID
     */
    private Integer userId;
    /**
     * 签到/签退时间
     */
    private LocalDateTime time;
    /**
     * 备注内容
     */
    private String remark;
    /**
     * 工作时间表Id
     */
    private Integer workTimeId;
    /**
     * 是否是签到请求
     */
    private Boolean isSignIn;

    public SignRequest() {
    }

    public SignRequest(Integer userId, LocalDateTime time, String remark, Integer workTimeId, Boolean isSignIn) {
        this.userId = userId;
        this.time = time;
        this.remark = remark;
        this.workTimeId = workTimeId;
        this.isSignIn = isSignIn;
    }
}
