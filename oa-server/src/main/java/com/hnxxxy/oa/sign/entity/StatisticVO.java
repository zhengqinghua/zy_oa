package com.hnxxxy.oa.sign.entity;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author 1112
 * 2022/5/11
 * 此对象作为StatisticsData返回的视图对象
 */
@Data
public class StatisticVO {
    /**
     * 用户统计信息列表
     */
    private List<UserStatisticDTO> dataList;
    /**
     * 请求用户姓名
     */
    private String requestUserName;
    /**
     * 请求时间
     */
    private Date requestTime;
}
