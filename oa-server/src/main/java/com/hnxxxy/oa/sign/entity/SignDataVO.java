package com.hnxxxy.oa.sign.entity;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author 1112
 * 2022/5/12
 * 本对象作为签到信息查询的视图对象使用
 */
@Data
public class SignDataVO {

    /**
     * 用户Id
     */
    private Integer userId;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 真实姓名
     */
    private String actualName;
    /**
     * 用户部门名
     */
    private String departmentName;
    /**
     * 用户机构名
     */
    private String mechanismName;

    /**
     * 签到备注
     */
    private String remark;

    /**
     *
     */
    private LocalDateTime signTime;

    /**
     * 签到标记
     */
    private Integer tag;
}
