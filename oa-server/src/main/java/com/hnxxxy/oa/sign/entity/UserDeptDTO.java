package com.hnxxxy.oa.sign.entity;

import lombok.Data;

/**
 * @author 1112
 * 2022/5/11
 * 此对象作为构造SignInfo对象数据传输对象
 */
@Data
public class UserDeptDTO {
    /**
     * 用户名
     */
    private String userName;
    /**
     * 真实姓名
     */
    private String actualName;
    /**
     * 用户部门名
     */
    private String departmentName;
    /**
     * 用户机构名
     */
    private String mechanismName;
}
