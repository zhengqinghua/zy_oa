package com.hnxxxy.oa.sign.entity;

import lombok.Data;

/**
 * @author 1112
 * 2022/5/11
 * 此对象用于构造StatisticsVO对象数据传输
 */

@Data
public class UserStatisticDTO {

    /**
     * 用户真实姓名
     */
    private String actualName;

    /**
     * 用户迟到率
     * Note:
     * 小数作为整形存储,除以100可得到原数
     */
    private Integer attendanceRate;

    /**
     * 迟到次数
     */
    private Integer lateTimes;

    /**
     * 早退次数
     */
    private Integer leaveEarlyTimes;

    /**
     * 旷工次数
     */
    private Integer absenteeismTimes;

    /**
     * 用户部门Id
     */
    private Integer departmentId;

    /**
     * 用户机构Id
     */
    private Integer mechanismId;

    /**
     * 总工作日天数
     */
    private Integer workDays;

}
