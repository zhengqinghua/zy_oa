package com.hnxxxy.oa.sign.controller;

import com.hnxxxy.oa.enums.BizCodeEnum;
import com.hnxxxy.oa.sign.entity.CheckSignVO;
import com.hnxxxy.oa.sign.entity.SignInfoVO;
import com.hnxxxy.oa.sign.request.SignCheckRequest;
import com.hnxxxy.oa.sign.request.SignRequest;
import com.hnxxxy.oa.sign.service.SignService;
import com.hnxxxy.oa.system.model.SysUserDO;
import com.hnxxxy.oa.utils.R;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author 1112
 * 2022/5/8
 */
@RestController
@RequestMapping("/apis")
@Api(value = "signController", tags = {"考勤模块"})
public class SignController extends BaseController {
    private final SignService service;
    private final Logger logger;

    @Autowired
    public SignController(SignService service) {
        this.service = service;
        this.logger = LoggerFactory.getLogger(SignController.class);
    }

    @RequestMapping(value = "/sign", method = RequestMethod.POST)
    public R sign(
            @RequestParam String remark,
            @RequestParam Integer workTimeId,
            @RequestParam Boolean isSignIn) {
        //获取签到用户信息
        SysUserDO userData = getCurrUserData();
        //获取当前时间
        LocalDateTime signTime = LocalDateTime.now();
        SignRequest request = new SignRequest(userData.getId(), signTime, remark, workTimeId, isSignIn);
        //记录参数
        logger.info("<---" + request);
        int status;
        SignInfoVO signInfoVO;
        // 根据是否为登录请求,执行不同的操作
        try {
            if (isSignIn) {
                status = service.signIn(request);
            } else {
                status = service.signOut(request);
            }
        } catch (ParseException e) {
            logger.error("请求转换失败\n失败数据:" + request);
            return R.buildError("API内部错误");
        }
        // 根据状态码返回
        /*
         *  code 1 登录/登出操作成功,返回对应视图对象
         *  code 0 / -1 操作重复, 返回操作重复
         * */
        switch (status) {
            case 0:
            case -1:
                return R.buildResult(BizCodeEnum.OPS_REPEAT);
            case 1:
                // 根据请求内容创建待返回的视图对象
                signInfoVO = service.getSignInfo(request);
                //返回视图对象
                return R.buildSuccess(signInfoVO);
            default:
                return R.buildError("API未知状态码");
        }
    }

    @RequestMapping(value = "/sign/check", method = RequestMethod.POST)
    public R checkSign(@RequestParam String queryTime) {
        //获取session当前签到的用户信息
        SysUserDO userData = getCurrUserData();
        Date time;
        try {
            //转换请求中的时间字符串
            time = SimpleDateFormat.getDateInstance().parse(queryTime);
        } catch (ParseException e) {
            logger.error("时间转换失败res:" + queryTime);
            return R.buildError("时间格式错误");
        }
        // 构造检查登录状态请求对象
        SignCheckRequest request = new SignCheckRequest(userData.getId(), time);
        //记录参数
        logger.info("<---" + request);
        // 检查当前用户的签到状态
        int status = service.checkSign(request);
        //构造待返回的视图对象
        CheckSignVO result = new CheckSignVO();
        result.setCurrUser(userData);
        result.setStatus(status);

        return R.buildSuccess(result);


    }
}
