package com.hnxxxy.oa.sign.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author 1112
 * 2022/5/8
 * 此对象作为与数据库Sign表的数据对象
 */
@Data
@TableName("sign")
public class SignDO implements Serializable {
    /**
     * 签卡Id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户Id
     */
    @TableField(value = "user_id")
    private Integer userId;
    /**
     * 签卡备注
     */
    private String remark;
    /**
     * 签卡时间
     */
    private LocalDateTime time;
    /**
     * 签卡标记
     */
    private Integer tag;
    /**
     * 创建日期
     */
    @TableField(value = "create_time")
    private Date createTime;

    private Integer workTimeId;
}
