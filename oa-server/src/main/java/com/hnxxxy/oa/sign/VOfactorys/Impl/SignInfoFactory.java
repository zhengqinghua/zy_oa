package com.hnxxxy.oa.sign.VOfactorys.Impl;

import com.hnxxxy.oa.sign.VOfactorys.VOFactory;
import com.hnxxxy.oa.sign.entity.SignDataVO;
import com.hnxxxy.oa.sign.entity.SignInfoVO;
import com.hnxxxy.oa.sign.entity.UserDeptDTO;
import com.hnxxxy.oa.sign.mapper.SignMapper;
import com.hnxxxy.oa.sign.request.SearchDataRequest;
import com.hnxxxy.oa.sign.request.SignRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author 1112
 * 2022/5/11
 */

@Component
public class SignInfoFactory implements VOFactory<SignInfoVO, SignRequest> {
    private final SignMapper mapper;

    @Autowired
    public SignInfoFactory(SignMapper signMapper) {
        mapper = signMapper;

    }

    @Override
    public SignInfoVO build(SignRequest signRequest) {
        SignInfoVO signInfoVO = new SignInfoVO();
        UserDeptDTO userDept = mapper.findUserDept(signRequest.getUserId());

        // 设置VO对象属性
        signInfoVO.setUsername(userDept.getUserName());
        signInfoVO.setActualName(userDept.getActualName());
        signInfoVO.setDepartmentName(userDept.getDepartmentName());
        signInfoVO.setMechanismName(userDept.getMechanismName());
        signInfoVO.setSignTime(signRequest.getTime());
        signInfoVO.setSignInfo(signRequest.getRemark());

        return signInfoVO;

    }

    public List<SignDataVO> buildBatch(SearchDataRequest request) {

        return mapper.searchSignData(request);
    }

}
