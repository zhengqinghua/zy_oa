package com.hnxxxy.oa.sign.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author 1112
 * 2022/5/8
 * 此对象作为与数据库workTime表交互的数据对象
 */
@Data
@TableName("work_time")
public class WorktimeDO {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("on_duty_time")
    private LocalDateTime onDutyTime;

    @TableField("off_duty_time")
    private LocalDateTime offDutyTime;

    @TableField(value = "create_time")
    private LocalDateTime createTime;
}
