package com.hnxxxy.oa.personnel.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hnxxxy.oa.personnel.model.DepartmentDO;
import com.hnxxxy.oa.personnel.request.DepartmentPageRequest;
import com.hnxxxy.oa.personnel.request.DepartmentRequest;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lzc
 * @since 2022-05-06
 */
public interface DepartmentService extends IService<DepartmentDO> {
    IPage<DepartmentDO> selectPageText(Page<DepartmentDO> page, DepartmentPageRequest departmentPageRequest);
    List<DepartmentDO> selectDepartment();
    DepartmentDO selectDepartmentById(int bid);
    boolean deleteDepartmentById(int id);
    boolean addDepartment(DepartmentRequest departmentRequest );
    boolean updateDepartmentById(DepartmentRequest departmentRequest);

    List<DepartmentDO> findMechanismIdDepartmentList(int id);
}
