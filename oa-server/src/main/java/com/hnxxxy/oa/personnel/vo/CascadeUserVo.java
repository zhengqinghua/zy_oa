package com.hnxxxy.oa.personnel.vo;

import lombok.Data;

@Data
public class CascadeUserVo {
    /**
     * 员工id
     */
    private int id;
    /**
     * 员工姓名
     */
    private String actualName;



    /**
     * 机构id
     */
    // private int mechanismId;

    /**
     * 部门id
     */
    // private int departId;


}
