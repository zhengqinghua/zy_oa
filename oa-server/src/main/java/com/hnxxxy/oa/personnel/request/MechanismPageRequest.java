package com.hnxxxy.oa.personnel.request;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 机构请求对象
 * </p>
 *
 * @author lzc
 * @since 2022-04-24
 */
import java.util.Date;
@Data
@ApiModel(value = "机构",description = "机构请求对象")
public class MechanismPageRequest {
    /**
     * 机构id
     */
    @ApiModelProperty(value = "机构id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 机构名称
     */
    @ApiModelProperty(value = "机构名称")
    private String name;
    /**
     * 机构简称
     */
    @ApiModelProperty(value = "机构简称")
    private String shortName;

    /**
     * 请求页数
     */
    @ApiModelProperty(value = "页数大小")
    private  int current;
    /**
     * 页面大小
     */
    @ApiModelProperty(value = "当前页面")
    private int size;

}

