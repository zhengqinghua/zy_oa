package com.hnxxxy.oa.personnel.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hnxxxy.oa.information.model.MessageDO;
import com.hnxxxy.oa.personnel.model.DepartmentDO;
import com.hnxxxy.oa.personnel.mapper.DepartmentMapper;
import com.hnxxxy.oa.personnel.request.DepartmentPageRequest;
import com.hnxxxy.oa.personnel.request.DepartmentRequest;
import com.hnxxxy.oa.personnel.service.DepartmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hnxxxy.oa.system.constant.UserConstant;
import com.hnxxxy.oa.system.model.SysUserDO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lzc
 * @since 2022-05-06
 */
@Service
public class DepartmentServiceImpl extends ServiceImpl<DepartmentMapper, DepartmentDO> implements DepartmentService {
    @Autowired
    DepartmentMapper departmentMapper;
    /**
     * 分页查询部门信息
     * */
    @Transactional
    @Override
    public IPage<DepartmentDO> selectPageText(Page<DepartmentDO> page, DepartmentPageRequest departmentRequest) {
        LambdaQueryWrapper<DepartmentDO> departmentLambdaQueryWrapper= Wrappers.lambdaQuery();
        if (departmentRequest.getId()!=null){
            departmentLambdaQueryWrapper.like(DepartmentDO::getUserId , departmentRequest.getId());
        }
        if(departmentRequest.getContactNumber()!=null){
            departmentLambdaQueryWrapper.like(DepartmentDO::getContactNumber , departmentRequest.getContactNumber());
        }
        if(departmentRequest.getFaxes()!=null){
            departmentLambdaQueryWrapper.eq(DepartmentDO::getFaxes , departmentRequest.getFaxes());
        }
        if(departmentRequest.getMechanismId()!=null){
            departmentLambdaQueryWrapper.eq(DepartmentDO::getMechanismId,departmentRequest.getMechanismId());}
        if(departmentRequest.getMobilePhone()!=null){
             departmentLambdaQueryWrapper.eq(DepartmentDO::getMobilePhone,departmentRequest.getMobilePhone()); }
        if(departmentRequest.getName()!=null){
            departmentLambdaQueryWrapper.like(DepartmentDO::getName,departmentRequest.getName());
        }
        if(departmentRequest.getUserId()!=null){
            departmentLambdaQueryWrapper.eq(DepartmentDO::getUserId,departmentRequest.getUserId());
        }
        return departmentMapper.selectPage(page,departmentLambdaQueryWrapper);
    }
    /**
     * 查询所有部门信息
     * */
    @Transactional
    @Override
    public List<DepartmentDO> selectDepartment() {
        return departmentMapper.selectList(null);
    }
    /**
     * 通过查询部门信息
     * */
    @Transactional
    @Override
    public DepartmentDO selectDepartmentById(int bid) {
        return departmentMapper.selectById(bid);
    }
    /**
     * 删除部门信息
     * */
    @Transactional
    @Override
    public boolean deleteDepartmentById(int id) {
        int i=departmentMapper.deleteById(id);
        if (i == 0) return false;
        return true;
    }
    /**
     *添加部门信息
     * */
    @Transactional
    @Override
    public boolean addDepartment(DepartmentRequest departmentRequest) {
        DepartmentDO departmentDO=new DepartmentDO();
        System.out.println(departmentDO.toString());
        departmentDO.setCreateTime(new Date());
        BeanUtils.copyProperties(departmentRequest, departmentDO);
        int  i= departmentMapper.insert(departmentDO);
        if (i == 0) return false;
        return true;
    }
    /**
     * 更新部门信息
     * */
    @Transactional
    @Override
    public boolean updateDepartmentById(DepartmentRequest departmentRequest) {
        DepartmentDO departmentDO=new DepartmentDO();
        departmentDO.setCreateTime(new Date());
        BeanUtils.copyProperties(departmentRequest, departmentDO);
        int i=departmentMapper.updateById(departmentDO);
        if (i == 0) return false;
        return true;
    }

    /**
     * 根据机构id查询部门列表
     * @param id
     * @return
     */
    @Override
    public List<DepartmentDO> findMechanismIdDepartmentList(int id) {
        List<DepartmentDO> departmentDOList = departmentMapper.selectList(new QueryWrapper<DepartmentDO>().eq("mechanism_id", id));
        return departmentDOList;
    }
}
