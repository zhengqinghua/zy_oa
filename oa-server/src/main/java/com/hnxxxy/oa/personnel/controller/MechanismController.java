package com.hnxxxy.oa.personnel.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hnxxxy.oa.personnel.model.MechanismDO;
import com.hnxxxy.oa.personnel.request.MechanismPageRequest;
import com.hnxxxy.oa.personnel.request.MechanismRequest;
import com.hnxxxy.oa.personnel.service.MechanismService;
import com.hnxxxy.oa.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * <p>
 *  人事/机构模块
 * </p>
 *
 * @author lzc
 * @since 2022-05-06
 */
@RestController
@RequestMapping("/personnel/mechanism")
@Api(value = "MechanismController", tags = {"机构模块"})
public class MechanismController {
    @Autowired
    MechanismService mechanismService;
    @ApiOperation(value = "分页查询机构信息")
    @PostMapping("/selectAllByPage")
    public R mechanismDOIPage(@RequestBody MechanismPageRequest mechanismPageRequest){
        Page<MechanismDO> departmentPage = new Page<>(mechanismPageRequest.getCurrent(), mechanismPageRequest.getSize());
        return R.buildSuccess(mechanismService.selectPageText(departmentPage,mechanismPageRequest));
    }
    @ApiOperation(value = "查询机构所有信息")
    @PostMapping("/selectAll")
    public R getMechanism(){
        List<MechanismDO> list=null;
        try{
            list= mechanismService.selectMechanism();
        }catch (Exception e){
            e.printStackTrace();
        }
        return R.buildSuccess(list);
    }
    @ApiOperation(value = "删除机构信息")
    @GetMapping("/deleteOne")
    public R deleteMechanismById(int id) {
        boolean b= mechanismService.deleteMechanismById(id);
        if(b)return R.buildSuccess("删除成功");
        return R.buildError("删除失败");
    }
    @ApiOperation(value = "查询机构信息")
    @GetMapping("/selectOne")
    public  R getMechanismOne(int id) {
        MechanismDO mechanismDO= mechanismService.selectMechanismById(id);
        if(mechanismDO==null) return R.buildError("未找到数据");
        return R.buildSuccess(mechanismDO);
    }
    @ApiOperation(value = "添加机构信息")
    @PostMapping("/addOne")
    public R addMechanismOne(@RequestBody MechanismRequest mechanismRequest) {
        boolean b= mechanismService.addMechanism(mechanismRequest);
        if(b)return R.buildSuccess("添加成功");
        return R.buildError("添加失败");
    }
    @ApiOperation(value = "更新机构信息")
    @PostMapping("/updateOne")
    public R updateMechanismOne(@RequestBody MechanismRequest mechanismRequest) {
        boolean b= mechanismService.updateMechanismById(mechanismRequest);
        if(b)return R.buildSuccess("修改成功");
        return R.buildError("修改失败");
    }
}
