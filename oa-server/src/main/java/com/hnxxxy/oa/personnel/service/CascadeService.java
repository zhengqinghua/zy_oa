package com.hnxxxy.oa.personnel.service;

import com.hnxxxy.oa.personnel.request.CascadeRequest;
import com.hnxxxy.oa.personnel.vo.CascadeMechanismVo;
import com.hnxxxy.oa.personnel.vo.CascadeUserVo;
import com.hnxxxy.oa.system.model.SysUserDO;

import java.util.List;

public interface CascadeService {
    List<CascadeUserVo> getUser(CascadeRequest cascadeRequest);
    List<CascadeMechanismVo> getDepartment();

    List<CascadeUserVo> getToUser(CascadeRequest cascadeRequest);
}
