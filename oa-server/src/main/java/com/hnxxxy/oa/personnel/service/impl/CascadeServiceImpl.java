package com.hnxxxy.oa.personnel.service.impl;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import com.hnxxxy.oa.information.model.MessageDO;
import com.hnxxxy.oa.information.vo.MessageVO;
import com.hnxxxy.oa.personnel.mapper.CascadeMapper;
import com.hnxxxy.oa.personnel.mapper.DepartmentMapper;
import com.hnxxxy.oa.personnel.mapper.MechanismMapper;
import com.hnxxxy.oa.personnel.model.DepartmentDO;
import com.hnxxxy.oa.personnel.model.MechanismDO;
import com.hnxxxy.oa.personnel.request.CascadeRequest;
import com.hnxxxy.oa.personnel.service.CascadeService;
import com.hnxxxy.oa.personnel.vo.CascadeDepartmentVo;
import com.hnxxxy.oa.personnel.vo.CascadeMechanismVo;
import com.hnxxxy.oa.personnel.vo.CascadeUserVo;
import com.hnxxxy.oa.system.mapper.SysUserMapper;
import com.hnxxxy.oa.system.model.SysUserDO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class CascadeServiceImpl implements CascadeService {
    @Autowired
    DepartmentMapper departmentMapper;
    @Autowired
    MechanismMapper mechanismMapper;
    @Autowired
    CascadeMapper cascadeMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Transactional
    public List<CascadeUserVo> getUser(CascadeRequest cascadeRequest) {

        //全局异常：int,未处理，通过id查询不行
        /*String string=""+cascadeRequest.getId();
        if(string!=null) return cascadeMapper.selectUserById(cascadeRequest);*/
        return cascadeMapper.selectUserByName(cascadeRequest);
    }
    @Transactional
    @Override
    public List<CascadeMechanismVo> getDepartment() {

        /**
         *
         * 保存机构信息
         * */
        List<CascadeMechanismVo> cascadeMechanismVos=new ArrayList<>();
        /**
         *查询机构信息
         * */
        List<MechanismDO> mechanismDO =mechanismMapper.selectList(null);
        /**
         *
         *迭代生成机构对象
         * */
        for(MechanismDO m:mechanismDO){
            CascadeMechanismVo cascadeMechanismVo=new CascadeMechanismVo();
            cascadeMechanismVo.setId(m.getId());
            cascadeMechanismVo.setName(m.getName());
            /**
             *
             * 查询部门信息
             * */
            LambdaQueryWrapper<DepartmentDO> departmentDOLambdaQueryWrapper= Wrappers.lambdaQuery();
            departmentDOLambdaQueryWrapper.eq(DepartmentDO::getMechanismId,m.getId());
            List<DepartmentDO> departmentDO=departmentMapper.selectList(departmentDOLambdaQueryWrapper);
            List<CascadeDepartmentVo> departmentVos=new ArrayList<>();
            /**
             *
             * 迭代生成部门对象
             * */
            for(DepartmentDO d:departmentDO)
            {   CascadeDepartmentVo cascadeDepartmentVo=new CascadeDepartmentVo();
                cascadeDepartmentVo.setId( d.getId());
                cascadeDepartmentVo.setName(d.getName());
                departmentVos.add(cascadeDepartmentVo);
            }
            cascadeMechanismVo.setList(departmentVos);
            cascadeMechanismVos.add(cascadeMechanismVo);
        }
        return cascadeMechanismVos;
    }

    /**
     * 根据条件查找用户
     * @param cascadeRequest
     * @return
     */
    @Override
    public List<CascadeUserVo> getToUser(CascadeRequest cascadeRequest) {

        Integer mechanismId = cascadeRequest.getMechanismId();
        Integer departId = cascadeRequest.getDepartId();
        Integer id = cascadeRequest.getId();
        String username = cascadeRequest.getUsername();

        // QueryWrapper<SysUserDO> queryWrapper = new QueryWrapper<SysUserDO>();
        QueryWrapper<SysUserDO> queryWrapper = new QueryWrapper<SysUserDO>().eq("depart_id", departId);

        if (!StrUtil.hasEmpty(username)){
            queryWrapper.like("name", username);
        }

        if (id != null){
            queryWrapper.like("id", id);
        }

        List<CascadeUserVo> cascadeUserVoList = sysUserMapper.selectList(queryWrapper)
                .stream().map(obj -> beanProocess(obj)).collect(Collectors.toList());


        return cascadeUserVoList;
    }

    private CascadeUserVo beanProocess(SysUserDO obj) {
        CascadeUserVo cascadeUserVo = new CascadeUserVo();
        BeanUtils.copyProperties(obj, cascadeUserVo);
        return cascadeUserVo;
    }
}


