package com.hnxxxy.oa.personnel.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author lzc
 * @since 2022-05-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("department")
public class DepartmentDO implements Serializable {

    private static final long serialVersionUID = 1L;
      @TableId(value = "id", type = IdType.AUTO)
    /**
       * 部门Id
       */
    private Integer id;
    /**
     * 部门名称
     */
    private String name;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 联系电话
     */
    private  Long contactNumber;
    /**
     * 手机号码
     */
    private Long mobilePhone;
    /**
     * 传真
     */
    private Integer faxes;
    /**
     * 机构id
     */
    private Integer mechanismId;
    /**
     * 创建时间
     */
    private Date createTime;


}
