package com.hnxxxy.oa.personnel.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hnxxxy.oa.personnel.model.MechanismDO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hnxxxy.oa.personnel.request.MechanismPageRequest;
import com.hnxxxy.oa.personnel.request.MechanismRequest;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lzc
 * @since 2022-05-06
 */
public interface MechanismService extends IService<MechanismDO> {
     IPage<MechanismDO> selectPageText(Page<MechanismDO> page, MechanismPageRequest mechanismPageRequest);
     List<MechanismDO> selectMechanism();
     MechanismDO selectMechanismById(int bid);
     boolean deleteMechanismById(int id);
     boolean addMechanism(MechanismRequest mechanismRequest);
     boolean updateMechanismById(MechanismRequest mechanismRequest);
}
