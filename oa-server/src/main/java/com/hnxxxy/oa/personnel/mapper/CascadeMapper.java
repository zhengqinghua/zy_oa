package com.hnxxxy.oa.personnel.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hnxxxy.oa.personnel.request.CascadeRequest;
import com.hnxxxy.oa.personnel.vo.CascadeUserVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
@Repository
public interface CascadeMapper  {
    @Select("select department.mechanism_id,sys_user.depart_id,sys_user.id,sys_user.username\n" +
            "        FROM\n" +
            "       mechanism\n" +
            "        LEFT JOIN department ON department.mechanism_id = mechanism.id\n" +
            "        LEFT JOIN sys_user ON  department.id= sys_user.depart_id\n" +
            "        having 1=1\n" +
            "            and  mechanism_id=#{mechanismId}\n" +
            "            and depart_id=#{departId}" +
            "            and  username like concat('%',#{username},'%')\n" +
            "            \n")
    List<CascadeUserVo> selectUserByName(CascadeRequest cascadeRequest);

    @Select("select department.mechanism_id,sys_user.depart_id,sys_user.id,sys_user.username\n" +
            "        FROM\n" +
            "       mechanism\n" +
            "        LEFT JOIN department ON department.mechanism_id = mechanism.id\n" +
            "        LEFT JOIN sys_user ON  department.id= sys_user.depart_id\n" +
            "        having 1=1\n" +
            "            and  mechanism_id=#{mechanismId}\n" +
            "            and depart_id=#{departId}" +
             "            and id=#{id}\n"+
            "            and  username like concat('%',#{username},'%')\n" +
            "            \n")
    List<CascadeUserVo> selectUserById(CascadeRequest cascadeRequest);
}
