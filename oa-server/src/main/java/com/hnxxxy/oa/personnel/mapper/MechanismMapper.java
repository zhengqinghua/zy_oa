package com.hnxxxy.oa.personnel.mapper;

import com.hnxxxy.oa.personnel.model.MechanismDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lzc
 * @since 2022-05-06
 */
@Mapper
@Repository
public interface MechanismMapper extends BaseMapper<MechanismDO> {

}
