package com.hnxxxy.oa.personnel.vo;

import lombok.Data;
import java.util.List;

@Data
public class CascadeMechanismVo {
    /**
     * 机构id
     * */
    private  int id;
    /**
     * 机构名称
     * */
    private  String name;
    /**
     * 部门
     * */
    private List<CascadeDepartmentVo> list;
}
