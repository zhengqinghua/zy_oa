package com.hnxxxy.oa.personnel.controller;

import com.hnxxxy.oa.personnel.request.CascadeRequest;
import com.hnxxxy.oa.personnel.service.CascadeService;
import com.hnxxxy.oa.personnel.vo.CascadeUserVo;
import com.hnxxxy.oa.system.model.SysUserDO;
import com.hnxxxy.oa.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/personnel/cascade")
@Api(value = "Cascade", tags = {"机构部门级联"})
public class CascadeController {
    @Autowired
    CascadeService cascadeService;

    @PostMapping("/CascadeToUser")
    @ApiOperation("级联查询用户信息")
    public R getCascadeUser(CascadeRequest cascadeRequest){
        List<CascadeUserVo> cascadeVo=cascadeService.getUser(cascadeRequest);
        if(cascadeVo==null) return R.buildError("未找到数据");
        return R.buildSuccess(cascadeVo);
    }
    @PostMapping("/CascadeToMechanism")
    @ApiOperation("级联加载机构部门信息")
    public R getCascadeOnload(){

        return R.buildSuccess(cascadeService.getDepartment());
    }

    @PostMapping("/getToUser")
    @ApiOperation("根据条件查询用户信息")
    public R getUser(@RequestBody CascadeRequest cascadeRequest){
        List<CascadeUserVo> cascadeUserVoList=cascadeService.getToUser(cascadeRequest);
        // if(cascadeUserVoList==null) return R.buildError("未找到数据");
        return R.buildSuccess(cascadeUserVoList);
    }
}
