package com.hnxxxy.oa.personnel.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author lzc
 * @since 2022-05-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("mechanism")
public class MechanismDO implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 机构id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 机构名称
     */
    private String name;
    /**
     * 机构简称
     */
    private String shortName;
    /**
     * 机构创建时间
     */
    private Date createTime;


}
