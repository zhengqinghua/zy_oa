package com.hnxxxy.oa.personnel.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hnxxxy.oa.personnel.model.DepartmentDO;
import com.hnxxxy.oa.personnel.request.DepartmentPageRequest;
import com.hnxxxy.oa.personnel.request.DepartmentRequest;
import com.hnxxxy.oa.personnel.service.DepartmentService;
import com.hnxxxy.oa.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/personnel/department")
@Api(value = "DepartmentController", tags = {"部门模块"})
public class DepartmentController {
    @Autowired
    DepartmentService departmentService;

    /**
     * <p>
     *  人事/部门模块
     * </p>
     *
     * @author lzc
     * @since 2022-05-06
     */
    @ApiOperation(value = "分页查询部门信息")
    @PostMapping("/selectByPage")
    public R departmentDOIPage(@RequestBody DepartmentPageRequest departmentPageRequest){
        Page<DepartmentDO> departmentPage = new Page<>(departmentPageRequest.getCurrent(),departmentPageRequest.getSize());
        return R.buildSuccess(departmentService.selectPageText(departmentPage,departmentPageRequest));
    }
    @ApiOperation(value = "查询部门所有信息")
    @PostMapping("/selectAll")
    public R getDepartmentAll(){
        List<DepartmentDO> list=null;
        try{
            list=departmentService.selectDepartment();
        }catch (Exception e){
            e.printStackTrace();
        }
        return R.buildSuccess(list);
    }
    @ApiOperation(value = "删除部门信息")
    @GetMapping("/deleteOne")
    public R deleteDepartmentById(int id) {
        boolean b=departmentService.deleteDepartmentById(id);
        if(b)return R.buildSuccess("删除成功") ;
        return R.buildError("删除失败");
    }
    @ApiOperation(value = "查询部门信息")
    @GetMapping("/selectOne")
    public  R getDepartmentOne(int id) {
        DepartmentDO department=departmentService.selectDepartmentById(id);
        if(department==null) return R.buildError("未找到数据");
        return R.buildSuccess(department);
    }
    @ApiOperation(value = "添加部门信息")
    @PostMapping("/addOne")
    public R addDepartmentOne(@RequestBody DepartmentRequest departmentRequest) {
        boolean b=departmentService.addDepartment(departmentRequest);
        if(b)return R.buildSuccess("添加成功") ;
        return R.buildError("添加失败");
    }
    @ApiOperation(value = "更新部门信息")
    @PostMapping("/updateOne")
    public R updateDepartmentOne(@RequestBody DepartmentRequest departmentRequest) {
        boolean b=departmentService.updateDepartmentById(departmentRequest);
        if(b)return R.buildSuccess("更新成功") ;
        return R.buildError("更新失败");
    }

    @ApiOperation(value = "根据机构id查部门列表")
    @GetMapping("/findMechanismById/{id}")
    public R findMechanismIdDepartmentList(@ApiParam(value = "部门id",required = true)
                                           @PathVariable("id") int id) {
        List<DepartmentDO> departmentDOList = departmentService.findMechanismIdDepartmentList(id);
        return R.buildSuccess(departmentDOList);
    }
}
