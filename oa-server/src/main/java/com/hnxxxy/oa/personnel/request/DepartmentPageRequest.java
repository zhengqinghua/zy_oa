package com.hnxxxy.oa.personnel.request;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;
/**
 * <p>
 * 部门请求对象
 * </p>
 *
 * @author lzc
 * @since 2022-04-24
 */
@Data
@ApiModel(value = "部门",description = "部门请求对象")
public class DepartmentPageRequest {

    /**
     * 部门Id
     */
    @ApiModelProperty(value = "部门id",example = "1")
    private Integer id;
    /**
     * 部门名称
     */
    @ApiModelProperty(value = "部门名称")
    private String name;
    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id")
    private String userId;
    /**
     * 联系电话
     */
    @ApiModelProperty(value = "联系电话")
    private Long contactNumber;
    /**
     * 手机号码
     */
    @ApiModelProperty(value = "手机号码")
    private Long mobilePhone;
    /**
     * 传真
     */
    @ApiModelProperty(value = "传真")
    private Integer faxes;
    /**
     * 机构id
     */
    @ApiModelProperty(value = "机构id")
    private Integer mechanismId;

/**
 * 请求页数
*/
@ApiModelProperty(value = "页数大小")
    private  int current;
    /**
     * 页面大小
     */
    @ApiModelProperty(value = "当前页面")
    private int size;

}
