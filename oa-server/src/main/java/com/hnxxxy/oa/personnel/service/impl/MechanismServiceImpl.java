package com.hnxxxy.oa.personnel.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hnxxxy.oa.personnel.model.DepartmentDO;
import com.hnxxxy.oa.personnel.model.MechanismDO;
import com.hnxxxy.oa.personnel.mapper.MechanismMapper;
import com.hnxxxy.oa.personnel.request.MechanismPageRequest;
import com.hnxxxy.oa.personnel.request.MechanismRequest;
import com.hnxxxy.oa.personnel.service.MechanismService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hnxxxy.oa.personnel.mapper.MechanismMapper;
import com.hnxxxy.oa.personnel.model.MechanismDO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.security.provider.MD2;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lzc
 * @since 2022-05-06
 */
@Service
public class MechanismServiceImpl extends ServiceImpl<MechanismMapper, MechanismDO> implements MechanismService {

    @Autowired
    MechanismMapper mechanismMapper;
    /**
     * 分页查询所有机构信息
     * */
    @Transactional
    @Override
    public IPage<MechanismDO> selectPageText(Page<MechanismDO> page, MechanismPageRequest mechanismRequest) {
        LambdaQueryWrapper<MechanismDO> departmentLambdaQueryWrapper= Wrappers.lambdaQuery();
        if (mechanismRequest.getId()!=null){
            departmentLambdaQueryWrapper.eq(MechanismDO::getId ,mechanismRequest.getId()); }
        if (mechanismRequest.getName()!=null){
            departmentLambdaQueryWrapper.like(MechanismDO::getName ,mechanismRequest.getName()); }
        if (mechanismRequest.getShortName()!=null){
            departmentLambdaQueryWrapper.like(MechanismDO::getShortName ,mechanismRequest.getShortName()); }
        return mechanismMapper.selectPage(page,departmentLambdaQueryWrapper);

    }
    /**
     * 查询所有机构信息
     * */
    @Transactional
    @Override
    public List<MechanismDO> selectMechanism() {
        return mechanismMapper.selectList(null);
    }

    /**
     * 通过id查询机构信息
     * */
    @Transactional
    @Override
    public MechanismDO selectMechanismById(int bid) {
        return mechanismMapper.selectById(bid);
    }
    /**
     * 通过id删除机构信息
     * */
    @Transactional
    @Override
    public boolean deleteMechanismById(int id) {
        int i=mechanismMapper.deleteById(id);
        if (i == 0) return false;
        return true;
    }
    /**
     * 添加机构信息
     * */
    @Transactional
    @Override
    public boolean addMechanism(MechanismRequest mechanismRequest) {
        MechanismDO mechanismDO=new MechanismDO();
        mechanismDO.setCreateTime(new Date());
        BeanUtils.copyProperties(mechanismRequest, mechanismDO);
        int  i= mechanismMapper.insert(mechanismDO);
        if (i == 0) return false;
        return true;
    }
    /**
     * 更新机构信息
     * */
    @Transactional
    @Override
    public boolean updateMechanismById(MechanismRequest mechanismRequest) {
        MechanismDO mechanismDO=new MechanismDO();
        mechanismDO.setCreateTime(new Date());
        BeanUtils.copyProperties(mechanismRequest, mechanismDO);
        int i=mechanismMapper.updateById(mechanismDO);
        if (i == 0) return false;
        return true;
    }
}
