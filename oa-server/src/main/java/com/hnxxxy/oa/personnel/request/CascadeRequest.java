package com.hnxxxy.oa.personnel.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
@Data
@ApiModel(value = "级联查询",description = "部门请求对象")
public class CascadeRequest {
    /**
     * 员工id
     */
    @ApiModelProperty(value = "员工id")
    private Integer id;
    /**
     * 部门id
     */
    @ApiModelProperty(value = "部门id")
    @JsonProperty("department_id")
    private Integer departId;
    /**
     * 员工姓名
     */
    @ApiModelProperty(value = "员工姓名")
    private String username;
    /**
     * 机构id
     */
    @ApiModelProperty(value = "机构id")
    @JsonProperty("mechanism_id")
    private Integer mechanismId;



}
