package com.hnxxxy.oa.personnel.vo;

import lombok.Data;

@Data
public class CascadeDepartmentVo {
    /**
     * 部门id
     * */
    private  int id;
    /**
     * 部门名称
     * */
    private  String name;
}
