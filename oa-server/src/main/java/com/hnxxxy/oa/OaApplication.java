package com.hnxxxy.oa;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description:
 * @author: zqh
 * @date: 2022/4/20 11:37
 **/
@SpringBootApplication
@MapperScan(basePackages = {"com.hnxxxy.oa.*.mapper"})
public class OaApplication {
    public static void main(String[] args) {
        SpringApplication.run(OaApplication.class, args);
    }
}
