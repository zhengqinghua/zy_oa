package com.hnxxxy.oa.document.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 文件信息表
 * </p>
 *
 * @author housang
 * @since 2022-05-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("file")
public class FileDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 文件Id
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 文件名称
     */
    private String name;

    /**
     * 文件类型
     */
    private Integer typeId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 创建日期
     */
    private Date createTime;

    /**
     * 父节点Id
     */
    private Integer pid;

    /**
     * 文件路径
     */
    private String path;

    /**
     * 是否已删除。1：已删除、2：未删除
     */
    private Integer isDelete;


}
