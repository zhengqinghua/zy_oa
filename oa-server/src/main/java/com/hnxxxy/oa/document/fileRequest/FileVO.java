package com.hnxxxy.oa.document.fileRequest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author housang
 * @create 2022-05-16-17:07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileVO {
    private String name;
    private String path;
    private Integer typeId;
    private String remark;
    private String createBy;
    private String image;
}
