package com.hnxxxy.oa.document.service;

import com.hnxxxy.oa.document.fileRequest.FileVO;
import com.hnxxxy.oa.document.fileRequest.SearchTag;
import com.hnxxxy.oa.document.model.FileDO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 文件信息表 服务类
 * </p>
 *
 * @author housang
 * @since 2022-05-16
 */
public interface FileService extends IService<FileDO> {
    void add(FileVO var1);

    List<FileDO> search(String var1);

    FileDO findByPath(String var1);

    List<FileDO> findByPid(Integer var1);

    void update(int var1, FileVO var2);

    void delete(Integer var1);

    void recycle(Integer var1);

    List<FileDO> fileSearch(SearchTag var1);
}
