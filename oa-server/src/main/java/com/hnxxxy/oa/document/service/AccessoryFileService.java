package com.hnxxxy.oa.document.service;

import com.hnxxxy.oa.document.fileRequest.AccessoryFileVO;
import com.hnxxxy.oa.document.model.AccessoryFileDO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 附件文件表 服务类
 * </p>
 *
 * @author housang
 * @since 2022-05-16
 */
public interface AccessoryFileService extends IService<AccessoryFileDO> {
    void add(AccessoryFileVO var1);

    void delete(int var1);

    List<AccessoryFileDO> search(int var1);
}
