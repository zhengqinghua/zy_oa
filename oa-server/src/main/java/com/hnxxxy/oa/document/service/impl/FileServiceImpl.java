package com.hnxxxy.oa.document.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hnxxxy.oa.document.fileRequest.FileVO;
import com.hnxxxy.oa.document.fileRequest.SearchTag;
import com.hnxxxy.oa.document.model.FileDO;
import com.hnxxxy.oa.document.mapper.FileMapper;
import com.hnxxxy.oa.document.service.FileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 文件信息表 服务实现类
 * </p>
 *
 * @author housang
 * @since 2022-05-16
 */
@Service
@Slf4j
public class FileServiceImpl extends ServiceImpl<FileMapper, FileDO> implements FileService {
    @Autowired
    private FileMapper fileMapper;

    public FileServiceImpl() {
    }

    @Override
    public void add(FileVO fileVO) {
        FileDO fileDO = new FileDO();
        fileDO.setCreateTime(new Date());
        BeanUtils.copyProperties(fileVO, fileDO);
        String fileName = fileVO.getName();
        String filePath = fileVO.getPath();
        String PFilePath = filePath.substring(0, filePath.indexOf(fileName) - 1);
        FileDO byPath = this.findByPath(PFilePath);
        int pid = byPath.getId();
        fileDO.setPid(pid);
        fileDO.setIsDelete(2);
        File file = new File(fileDO.getPath());
        if (fileDO.getTypeId() == 1) {
            if (!file.exists()) {
                file.mkdir();
            }
        } else {
            try {
                file.createNewFile();
            } catch (IOException var10) {
                var10.printStackTrace();
            }
        }

        this.fileMapper.insert(fileDO);
    }

    @Override
    public List<FileDO> search(String path) {
        FileDO fileDO = this.findByPath(path);
        if (fileDO.getTypeId() == 1) {
            return this.findByPid(fileDO.getId());
        } else {
            List<FileDO> fileDOS = new ArrayList();
            fileDOS.add(fileDO);
            return fileDOS;
        }
    }

    @Override
    public FileDO findByPath(String path) {
        return (FileDO)this.fileMapper.selectOne((Wrapper)(new QueryWrapper()).eq("path", path));
    }

    @Override
    public List<FileDO> findByPid(Integer pid) {
        List<FileDO> Files = this.fileMapper.selectList((Wrapper)(new QueryWrapper()).eq("pid", pid));
        return Files;
    }

    @Override
    public void update(int id, FileVO fileVO) {
        FileDO file = (FileDO)this.fileMapper.selectById(id);
        BeanUtils.copyProperties(fileVO, file);
        Integer rows = this.fileMapper.updateET(file);
        log.info("修改文档:rows={}", rows);
    }

    @Override
    public void delete(Integer id) {
        FileDO fileDO = (FileDO)this.fileMapper.selectOne((Wrapper)(new QueryWrapper()).eq("id", id));
        File file = new File(fileDO.getPath());
        file.delete();
        int rows = this.fileMapper.deleteById(id);
        log.info("完全删除文档:rows={}", rows);
    }

    @Override
    public void recycle(Integer id) {
        Integer rows = this.fileMapper.updateIsDelete(id);
        log.info("回收文档:rows={}", rows);
    }

    @Override
    public List<FileDO> fileSearch(SearchTag searchTag) {
        return this.fileMapper.fileSearch(searchTag);
    }
}
