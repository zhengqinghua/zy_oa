package com.hnxxxy.oa.document.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.hnxxxy.oa.document.fileRequest.FileTypeVO;
import com.hnxxxy.oa.document.model.FileTypeDO;
import com.hnxxxy.oa.document.mapper.FileTypeMapper;
import com.hnxxxy.oa.document.service.FileTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 文件类型表 服务实现类
 * </p>
 *
 * @author housang
 * @since 2022-05-16
 */
@Service
@Slf4j
public class FileTypeServiceImpl extends ServiceImpl<FileTypeMapper, FileTypeDO> implements FileTypeService {
    @Autowired
    private FileTypeMapper fileTypeMapper;

    public FileTypeServiceImpl() {
    }

    @Override
    public void add(FileTypeVO fileTypeVO) {
        FileTypeDO fileTypeDO = new FileTypeDO();
        fileTypeDO.setCreateTime(new Date());
        BeanUtils.copyProperties(fileTypeVO, fileTypeDO);
        this.fileTypeMapper.insert(fileTypeDO);
    }

    @Override
    public List<FileTypeDO> findAll() {
        return this.fileTypeMapper.selectList((Wrapper)null);
    }

    @Override
    public void update(int id, FileTypeVO fileTypeVO) {
        FileTypeDO fileType = (FileTypeDO)this.fileTypeMapper.selectById(id);
        BeanUtils.copyProperties(fileTypeVO, fileType);
        Integer rows = this.fileTypeMapper.updateET(fileType);
        log.info("修改文档类型：rows={}", rows);
    }

    @Override
    public void delete(int id) {
        int rows = this.fileTypeMapper.deleteById(id);
        log.info("删除文档类型:rows={}", rows);
    }
}
