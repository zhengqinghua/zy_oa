package com.hnxxxy.oa.document.fileRequest;

/**
 * @author housang
 * @create 2022-05-16-17:07
 */
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchTag {
    private String fileName;
    private String createName;
    private Date beginTime;
    private Date endTime;
}
