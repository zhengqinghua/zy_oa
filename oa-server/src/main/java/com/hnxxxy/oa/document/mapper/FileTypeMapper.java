package com.hnxxxy.oa.document.mapper;

import com.hnxxxy.oa.document.model.FileTypeDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 文件类型表 Mapper 接口
 * </p>
 *
 * @author housang
 * @since 2022-05-16
 */
@Repository
public interface FileTypeMapper extends BaseMapper<FileTypeDO> {
    Integer updateET(FileTypeDO fileTypeDO);
}
