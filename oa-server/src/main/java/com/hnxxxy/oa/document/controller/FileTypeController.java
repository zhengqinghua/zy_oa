package com.hnxxxy.oa.document.controller;


import com.hnxxxy.oa.document.fileRequest.FileTypeVO;
import com.hnxxxy.oa.document.service.FileTypeService;
import com.hnxxxy.oa.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 文件类型表 前端控制器
 * </p>
 *
 * @author housang
 * @since 2022-05-16
 */
@RestController
@RequestMapping({"/document/fileType"})
@Api(
        value = "FileTypeController",
        tags = {"文档类型模块"}
)
public class FileTypeController {
    @Autowired
    private FileTypeService fileTypeService;

    public FileTypeController() {
    }

    @PostMapping({"/add"})
    @ApiOperation("增加文档类型")
    public R addFileType(@ApiParam("文件类型对象") @RequestBody FileTypeVO fileTypeVO) {
        this.fileTypeService.add(fileTypeVO);
        return R.buildSuccess();
    }

    @PostMapping({"/update/{id}"})
    @ApiOperation("修改文档类型")
    public R updateFileType(@ApiParam(value = "文档类型名称id",required = true) @PathVariable("id") int id, @ApiParam("文档类型名称对象") @RequestBody FileTypeVO fileTypeVO) {
        this.fileTypeService.update(id, fileTypeVO);
        return R.buildSuccess("文档修改成功");
    }

    @PostMapping({"/delete/{id}"})
    @ApiOperation("删除文档类型")
    public R deleteFileType(@ApiParam(value = "文档类型名称id",required = true) @PathVariable("id") int id) {
        this.fileTypeService.delete(id);
        return R.buildSuccess("文档删除成功");
    }
}


