package com.hnxxxy.oa.document.mapper;

import com.hnxxxy.oa.document.model.AccessoryFileDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 附件文件表 Mapper 接口
 * </p>
 *
 * @author housang
 * @since 2022-05-16
 */
@Repository
public interface AccessoryFileMapper extends BaseMapper<AccessoryFileDO> {

}
