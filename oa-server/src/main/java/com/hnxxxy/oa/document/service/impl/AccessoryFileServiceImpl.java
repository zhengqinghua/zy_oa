package com.hnxxxy.oa.document.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hnxxxy.oa.document.fileRequest.AccessoryFileVO;
import com.hnxxxy.oa.document.model.AccessoryFileDO;
import com.hnxxxy.oa.document.mapper.AccessoryFileMapper;
import com.hnxxxy.oa.document.service.AccessoryFileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 附件文件表 服务实现类
 * </p>
 *
 * @author housang
 * @since 2022-05-16
 */
@Service
@Slf4j
public class AccessoryFileServiceImpl extends ServiceImpl<AccessoryFileMapper, AccessoryFileDO> implements AccessoryFileService {
    @Autowired
    private AccessoryFileMapper accessoryFileMapper;

    public AccessoryFileServiceImpl() {
    }

    @Override
    public void add(AccessoryFileVO accessoryFileVO) {
        AccessoryFileDO accessoryFileDO = new AccessoryFileDO();
        accessoryFileDO.setCreateTime(new Date());
        BeanUtils.copyProperties(accessoryFileVO, accessoryFileDO);
        int rows = this.accessoryFileMapper.insert(accessoryFileDO);
        log.info("添加附加文档:rows={}", rows);
    }

    @Override
    public void delete(int id) {
        int rows = this.accessoryFileMapper.deleteById(id);
        log.info("删除附件：rows={}", rows);
    }

    @Override
    public List<AccessoryFileDO> search(int fileId) {
        return this.accessoryFileMapper.selectList((Wrapper)(new QueryWrapper()).eq("file_id", fileId));
    }
}
