package com.hnxxxy.oa.document.mapper;

import com.hnxxxy.oa.document.fileRequest.SearchTag;
import com.hnxxxy.oa.document.model.FileDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * <p>
 * 文件信息表 Mapper 接口
 * </p>
 *
 * @author housang
 * @since 2022-05-16
 */
@Repository
public interface FileMapper extends BaseMapper<FileDO> {
    Integer updateET(FileDO fileDO);

    Integer updateIsDelete(int id);

    List<FileDO> fileSearch(SearchTag searchTag);
}
