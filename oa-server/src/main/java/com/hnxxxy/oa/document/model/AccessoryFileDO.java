package com.hnxxxy.oa.document.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 附件文件表
 * </p>
 *
 * @author housang
 * @since 2022-05-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("accessory_file")
public class AccessoryFileDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 文件附件Id
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 文件Id
     */
    private Integer fileId;

    /**
     * 附件名称
     */
    private String name;

    /**
     * 附件大小
     */
    private Integer size;

    /**
     * 附件类型
     */
    private Integer fileTypeId;

    /**
     * 创建日期
     */
    private Date createTime;

    /**
     * 附件路径
     */
    private String path;


}
