package com.hnxxxy.oa.document.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 文件类型表
 * </p>
 *
 * @author housang
 * @since 2022-05-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("file_type")
public class FileTypeDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 文件类型id
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 文件类型名
     */
    private String name;

    /**
     * 文件类型对应的图标
     */
    private String image;

    /**
     * 文件类型后缀
     */
    private String suffix;

    /**
     * 创建日期
     */
    private Date createTime;


}
