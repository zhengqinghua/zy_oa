package com.hnxxxy.oa.document.fileRequest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author housang
 * @create 2022-05-16-17:07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileTypeVO {
    private String name;
    private String image;
    private String suffix;
}
