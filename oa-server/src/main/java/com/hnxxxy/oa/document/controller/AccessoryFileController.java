package com.hnxxxy.oa.document.controller;


import com.hnxxxy.oa.document.fileRequest.AccessoryFileVO;
import com.hnxxxy.oa.document.model.AccessoryFileDO;
import com.hnxxxy.oa.document.service.AccessoryFileService;
import com.hnxxxy.oa.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 附件文件表 前端控制器
 * </p>
 *
 * @author housang
 * @since 2022-05-16
 */
@RestController
@RequestMapping({"/document/accessoryFileDO"})
@Api(
        value = "AccessoryFileController",
        tags = {"文档附件模块"}
)
public class AccessoryFileController {
    @Autowired
    private AccessoryFileService accessoryFileService;

    public AccessoryFileController() {
    }

    @PostMapping({"/add"})
    @ApiOperation("增加附加文档")
    public R addAccessoryFile(@ApiParam("附加文档对象") AccessoryFileVO accessoryFileVO) {
        this.accessoryFileService.add(accessoryFileVO);
        return R.buildSuccess();
    }

    @PostMapping({"/delete/{id}"})
    @ApiOperation("删除附加文档")
    public R deleteAccessoryFile(@ApiParam(value = "附加文档id",required = true) @PathVariable("id") int id) {
        this.accessoryFileService.delete(id);
        return R.buildSuccess("删除成功");
    }

    @PostMapping({"/search{fileId}"})
    @ApiOperation("查找附加文档")
    public R searchAccessoryFile(@ApiParam(value = "文档id",required = true) @PathVariable("fileId") int fileId) {
        List<AccessoryFileDO> accessoryFileDOS = this.accessoryFileService.search(fileId);
        return R.buildSuccess(accessoryFileDOS);
    }
}


