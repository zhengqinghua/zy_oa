package com.hnxxxy.oa.document.service;

import com.hnxxxy.oa.document.fileRequest.FileTypeVO;
import com.hnxxxy.oa.document.model.FileTypeDO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 文件类型表 服务类
 * </p>
 *
 * @author housang
 * @since 2022-05-16
 */
public interface FileTypeService extends IService<FileTypeDO> {
    void add(FileTypeVO var1);

    List<FileTypeDO> findAll();

    void update(int var1, FileTypeVO var2);

    void delete(int var1);
}
