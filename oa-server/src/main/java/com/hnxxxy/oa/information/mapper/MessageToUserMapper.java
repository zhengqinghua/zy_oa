package com.hnxxxy.oa.information.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hnxxxy.oa.information.model.MessageToUserDO;

/**
 * <p>
 * 消息发送对象 Mapper 接口
 * </p>
 *
 * @author zqh
 * @since 2022-04-24
 */
public interface MessageToUserMapper extends BaseMapper<MessageToUserDO> {

}
