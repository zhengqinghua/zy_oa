package com.hnxxxy.oa.information.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 消息类型表
 * </p>
 *
 * @author zqh
 * @since 2022-04-24
 */
@ApiModel(value = "消息类型请求",description = "消息类型请求对象")
@Data
public class MessageTypePageRequest {

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页",example = "1")
    private Integer page;

    /**
     * 每页条数
     */
    @ApiModelProperty(value = "每页显示多少条",example = "10")
    private Integer size;

    /**
     * 消息类型名称
     */
    @ApiModelProperty(value = "消息类型名称",example = "java")
    private String name;

    /**
     * 消息类型描述
     */
    @ApiModelProperty(value = "消息类型描述",example = "java66666")
    private String bewrite;

}
