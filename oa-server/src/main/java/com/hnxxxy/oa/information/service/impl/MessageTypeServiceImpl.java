package com.hnxxxy.oa.information.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hnxxxy.oa.information.mapper.MessageTypeMapper;
import com.hnxxxy.oa.information.model.MessageTypeDO;
import com.hnxxxy.oa.information.request.MessageTypePageRequest;
import com.hnxxxy.oa.information.request.MessageTypeRequest;
import com.hnxxxy.oa.information.service.MessageTypeService;
import com.hnxxxy.oa.information.vo.MessageTypeVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 消息类型表 服务实现类
 * </p>
 *
 * @author zqh
 * @since 2022-04-24
 */
@Service
@Slf4j
public class MessageTypeServiceImpl implements MessageTypeService {


    @Autowired
    private MessageTypeMapper messageTypeMapper;

    /**
     * 新增消息类型
     * @param messageTypeRequest
     */
    @Override
    public void add(MessageTypeRequest messageTypeRequest) {
        MessageTypeDO messageTypeDO = new MessageTypeDO();
        messageTypeDO.setCreateTime(new Date());

        BeanUtils.copyProperties(messageTypeRequest, messageTypeDO);

        int rows = messageTypeMapper.insert(messageTypeDO);
        log.info("{},{}", rows,messageTypeDO);
    }

    /**
     * 删除消息类型
     * @param id
     */
    @Override
    public void delete(int id) {
        int rows = messageTypeMapper.delete(new QueryWrapper<MessageTypeDO>().eq("id", id));
        log.info("删除{}行", rows);
    }

    /**
     * 修改消息类型
     * @param id
     * @param messageTypeRequest
     */
    @Override
    public void update(MessageTypeRequest messageTypeRequest) {
        MessageTypeDO messageTypeDO = new MessageTypeDO();
        BeanUtils.copyProperties(messageTypeRequest, messageTypeDO);
        int rows = messageTypeMapper.updateById(messageTypeDO);
        log.info("修改消息:rows={}",rows);
    }

    /**
     * 查询全部消息类型
     * @return
     */
    @Override
    public List<MessageTypeVO> findAllMessageType() {
        List<MessageTypeDO> messageTypeDOList = messageTypeMapper.selectList(null);

        List<MessageTypeVO> messageVOList = messageTypeDOList.stream().map(obj -> {
            MessageTypeVO messageTypeVO = new MessageTypeVO();
            BeanUtils.copyProperties(obj, messageTypeVO);
            return messageTypeVO;
        }).collect(Collectors.toList());

        log.info("全部消息类型:messageVOList={}",messageVOList);
        return messageVOList;
    }

    @Override
    public Map<String, Object> page(MessageTypePageRequest messageTypePageRequest) {

        int page = messageTypePageRequest.getPage();
        int size = messageTypePageRequest.getSize();
        String name = messageTypePageRequest.getName();
        String bewrite = messageTypePageRequest.getBewrite();

        QueryWrapper<MessageTypeDO> queryWrapper = new QueryWrapper<MessageTypeDO>();

        if (!StrUtil.hasEmpty(name)){
            queryWrapper.like("name", name);
        }

        if (!StrUtil.hasEmpty(bewrite)){
            queryWrapper.like("bewrite",bewrite);
        }

        Page<MessageTypeDO> pageInfo = new Page<>(page, size);

        IPage<MessageTypeDO> messageTypeDOIPage = messageTypeMapper.selectPage(pageInfo,queryWrapper);

        Map<String,Object> pageMap = new HashMap<>(3);

        pageMap.put("total_record", messageTypeDOIPage.getTotal());
        pageMap.put("total_page", messageTypeDOIPage.getPages());
        pageMap.put("current_data",messageTypeDOIPage.getRecords()
                .stream().map(obj->beanProocess(obj)).collect(Collectors.toList()));

        log.info("分页消息:pageMap={}",pageMap);
        return pageMap;
    }

    /**
     * 根据id查询消息类型
     * @param id
     * @return
     */
    @Override
    public MessageTypeVO findByIdMessageType(int id) {
        MessageTypeVO messageTypeVO = new MessageTypeVO();

        MessageTypeDO messageTypeDO = messageTypeMapper.selectById(id);

        BeanUtils.copyProperties(messageTypeDO,messageTypeVO);

        return messageTypeVO;
    }

    private MessageTypeVO beanProocess(MessageTypeDO obj) {
        MessageTypeVO messageTypeVO = new MessageTypeVO();
        BeanUtils.copyProperties(obj, messageTypeVO);
        return messageTypeVO;
    }


}
