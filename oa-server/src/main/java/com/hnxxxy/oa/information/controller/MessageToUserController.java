package com.hnxxxy.oa.information.controller;


import com.hnxxxy.oa.information.request.MessageToUserRequest;
import com.hnxxxy.oa.information.service.MessageToUserService;
import com.hnxxxy.oa.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 消息发送对象 前端控制器
 * </p>
 *
 * @author zqh
 * @since 2022-04-24
 */
@RestController
@RequestMapping("/api/messagetouser/v1")
@Api(value = "MessageToUserController", tags = {"消息发送模块"})
public class MessageToUserController {

    @Autowired
    private MessageToUserService messageToUserService;

    /**
     * 新建消息
     * @param messageRequest
     * @return
     */
    @PostMapping("/send")
    @ApiOperation(value = "发送消息")
    public R send(@ApiParam("消息发送对象") @RequestBody MessageToUserRequest messageToUserRequest){
        messageToUserService.send(messageToUserRequest);
        return R.buildSuccess();
    }
}

