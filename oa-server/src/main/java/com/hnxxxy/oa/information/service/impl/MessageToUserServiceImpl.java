package com.hnxxxy.oa.information.service.impl;

import com.hnxxxy.oa.information.mapper.MessageToUserMapper;
import com.hnxxxy.oa.information.model.MessageToUserDO;
import com.hnxxxy.oa.information.request.MessageToUserRequest;
import com.hnxxxy.oa.information.service.MessageToUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * <p>
 * 消息发送对象 服务实现类
 * </p>
 *
 * @author zqh
 * @since 2022-04-24
 */
@Service
@Slf4j
public class MessageToUserServiceImpl implements MessageToUserService {

    @Autowired
    private MessageToUserMapper messageToUserMapper;

    /**
     * 发送消息
     * @param messageToUserRequest
     */
    @Override
    public void send(MessageToUserRequest messageToUserRequest) {
        MessageToUserDO messageToUserDO = new MessageToUserDO();

        // TODO 等待部门机构模块的接入

        BeanUtils.copyProperties(messageToUserRequest, messageToUserDO);
        messageToUserDO.setCreateTime(LocalDateTime.now());

        int rows = messageToUserMapper.insert(messageToUserDO);
        log.info("rows={},messageToUserDO={}", rows,messageToUserDO);

    }
}
