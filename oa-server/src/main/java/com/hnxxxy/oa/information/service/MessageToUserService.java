package com.hnxxxy.oa.information.service;

import com.hnxxxy.oa.information.request.MessageToUserRequest;

/**
 * <p>
 * 消息发送对象 服务类
 * </p>
 *
 * @author zqh
 * @since 2022-04-24
 */
public interface MessageToUserService {

    /**
     * 发送消息
     * @param messageToUserRequest
     */
    void send(MessageToUserRequest messageToUserRequest);
}
