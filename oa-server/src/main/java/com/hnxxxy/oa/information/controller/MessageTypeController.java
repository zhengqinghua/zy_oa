package com.hnxxxy.oa.information.controller;

import com.hnxxxy.oa.information.request.MessageTypePageRequest;
import com.hnxxxy.oa.information.request.MessageTypeRequest;
import com.hnxxxy.oa.information.service.MessageTypeService;
import com.hnxxxy.oa.information.vo.MessageTypeVO;
import com.hnxxxy.oa.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 消息类型表 前端控制器
 * </p>
 *
 * @author zqh
 * @since 2022-04-24
 */
@RestController
@RequestMapping("/api/messagetype/v1")
@Api(value = "MessageTypeController", tags = {"消息类型模块"})
public class MessageTypeController {

    @Autowired
    private MessageTypeService messageTypeService;


    @PostMapping("/add")
    @ApiOperation(value = "新建消息类型")
    public R add(@ApiParam("消息类型对象")@RequestBody MessageTypeRequest messageTypeRequest){

        messageTypeService.add(messageTypeRequest);

        return R.buildSuccess();
    }

    @ApiOperation(value = "删除消息类型名称")
    @GetMapping("/del/{id}")
    public R delete(@ApiParam(value = "消息类型id",required = true)
                    @PathVariable("id") int id){
        messageTypeService.delete(id);
        return R.buildSuccess();
    }

    @PostMapping("/update")
    @ApiOperation(value = "修改消息类型名称")
    public R update(@ApiParam("消息类型名称对象")
                    @RequestBody MessageTypeRequest messageTypeRequest){
        messageTypeService.update(messageTypeRequest);
        return R.buildSuccess();
    }

    @GetMapping("/findById/{id}")
    @ApiOperation(value = "根据id查询消息类型")
    public R findByIdMessageType(@ApiParam(value = "消息类型id",required = true)
                                 @PathVariable("id") int id ){
        MessageTypeVO messageTypeVO = messageTypeService.findByIdMessageType(id);

        return R.buildSuccess(messageTypeVO);
    }

    @PostMapping("/list")
    @ApiOperation(value = "查询全部信息消息")
    public R findAllMessageType(){
        List<MessageTypeVO> messageTypeVO = messageTypeService.findAllMessageType();
        return R.buildSuccess(messageTypeVO);
    }

    @ApiOperation("分页查询消息类型列表")
    @PostMapping("/page")
    public R pageMessageList(@ApiParam("消息类型分页对象") @RequestBody MessageTypePageRequest messageTypePageRequest){
        Map<String,Object> pageResult = messageTypeService.page(messageTypePageRequest);
        return R.buildSuccess(pageResult);
    }
}

