package com.hnxxxy.oa.information.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hnxxxy.oa.information.model.MessageDO;

/**
 * <p>
 * 消息表 Mapper 接口
 * </p>
 *
 * @author zqh
 * @since 2022-04-24
 */
public interface MessageMapper extends BaseMapper<MessageDO> {

}
