package com.hnxxxy.oa.information.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 * 消息表
 * </p>
 *
 * @author zqh
 * @since 2022-04-24
 */
@Data
public class MessageVO {


    /**
     * 消息Id
     */
    private Integer id;

    /**
     * 消息标题
     */
    private String title;

    /**
     * 消息内容
     */
    private String content;

    /**
     * 消息类型
     */
    private Integer type;

    /**
     * 开始有效时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss",locale = "zh",timezone = "GMT+8")
    @JsonProperty("begin_time")
    private LocalDateTime beginTime;

    /**
     * 有效结束时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss",locale = "zh",timezone = "GMT+8")
    @JsonProperty("end_time")
    private LocalDateTime endTime;

    /**
     * 发送者
     */
    @JsonProperty("from_user_id")
    private Integer fromUserid;

    /**
     * 发送对象
     */
    private Integer [] userList;

    /**
     * 是否已发布
     */
    @JsonProperty("is_publish")
    private Integer isPublish;

    /**
     * 发送时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss",locale = "zh",timezone = "GMT+8")
    @JsonProperty("record_time")
    private LocalDateTime recordTime;

    /**
     * 创建日期
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss",locale = "zh",timezone = "GMT+8")
    @JsonProperty("create_time")
    private LocalDateTime createTime;


}
