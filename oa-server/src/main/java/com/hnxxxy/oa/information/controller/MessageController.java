package com.hnxxxy.oa.information.controller;


import com.hnxxxy.oa.information.request.MessagePageRequest;
import com.hnxxxy.oa.information.request.MessageRequest;
import com.hnxxxy.oa.information.service.MessageService;
import com.hnxxxy.oa.information.vo.MessageVO;
import com.hnxxxy.oa.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * <p>
 * 消息表 前端控制器
 * </p>
 *
 * @author zqh
 * @since 2022-04-24
 */
@RestController
//api 指C端  message 模块  v1版本号
@RequestMapping("/api/message/v1")
@Api(value = "MessageController", tags = {"消息模块"})
public class MessageController {

    @Autowired
    private MessageService messageService;


    /**
     * 新建消息
     * @param messageRequest
     * @return
     */
    @PostMapping("/add")
    @ApiOperation(value = "创建消息")
    public R add(@ApiParam("消息对象") @RequestBody MessageRequest messageRequest){
        messageService.add(messageRequest);
        return R.buildSuccess("新增消息成功");
    }

    @ApiOperation(value = "删除消息")
    @PostMapping("/del/{id}")
    public R delete(@ApiParam(value = "消息id",required = true)
                    @PathVariable("id") int id){
        messageService.delete(id);
        return R.buildSuccess("删除消息成功");
    }

    @PostMapping("/update/{id}")
    @ApiOperation(value = "修改消息")
    public R update(@ApiParam(value = "消息id",required = true)
                    @PathVariable("id") int id,
                    @ApiParam("消息对象")
                    @RequestBody MessageRequest messageRequest){
        messageService.update(id,messageRequest);
        return R.buildSuccess("修改消息成功");
    }

    @PostMapping("/list")
    @ApiOperation(value = "查询全部信息消息")
    public R findAllMessage(){
        List<MessageVO> messageVO = messageService.findAllMessage();
        return R.buildSuccess(messageVO);
    }

    @PostMapping("/list/{id}")
    @ApiOperation(value = "根据id查询消息")
    public R findByIdMessage(@ApiParam(value = "消息id",required = true)
                             @PathVariable("id") int id){
        MessageVO messageVO = messageService.findByIdMessage(id);
        return R.buildSuccess(messageVO);
    }

    @ApiOperation("分页查询消息列表")
    @PostMapping("/page")
    public R pageMessageList(@ApiParam("消息分页对象") @RequestBody MessagePageRequest messagePageRequest){
        Map<String,Object> pageResult = messageService.page(messagePageRequest);
        return R.buildSuccess(pageResult);
    }

}

