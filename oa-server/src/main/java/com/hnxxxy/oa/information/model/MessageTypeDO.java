package com.hnxxxy.oa.information.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 消息类型表
 * </p>
 *
 * @author zqh
 * @since 2022-04-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("message_type")
public class MessageTypeDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 消息类型Id
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 消息类型名称
     */
    private String name;

    /**
     * 消息类型描述
     */
    private String bewrite;

    /**
     * 创建日期
     */
    private Date createTime;


}
