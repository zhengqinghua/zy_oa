package com.hnxxxy.oa.information.service;

import com.hnxxxy.oa.information.request.MessagePageRequest;
import com.hnxxxy.oa.information.request.MessageRequest;
import com.hnxxxy.oa.information.vo.MessageVO;

import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: zqh
 * @date: 2022/4/25 11:47
 **/
public interface MessageService {

    /**
     * 新增消息
     * @param messageRequest
     */
    void add(MessageRequest messageRequest);

    /**
     * 根据Id删除消息
     * @param id
     */
    void delete(int id);

    /**
     * 修改消息
     * @param messageRequest
     */
    void update(int id, MessageRequest messageRequest);

    /**
     * 根据id查询消息
     */
    MessageVO findByIdMessage(int id);

    /**
     * 查询所有的消息
     */
    List<MessageVO> findAllMessage();

    /**
     * 分页查询消息
     * @param page
     * @param size
     * @return
     */
    Map<String, Object> page(MessagePageRequest messagePageRequest);
}
