package com.hnxxxy.oa.information.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 * 消息表
 * </p>
 *
 * @author zqh
 * @since 2022-04-24
 */
@ApiModel(value = "消息请求",description = "消息请求对象")
@Data
public class MessagePageRequest {

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页",example = "1")
    private Integer page;

    /**
     * 每页条数
     */
    @ApiModelProperty(value = "每页显示多少条",example = "10")
    private Integer size;

    /**
     * 消息标题
     */
    @ApiModelProperty(value = "标题",example = "Java入门")
    private String title;

    /**
     * 消息内容
     */
    @ApiModelProperty(value = "内容",example = "Java888888888888888888888")
    private String content;

    /**
     * 消息类型
     */
    @ApiModelProperty(value = "类型",example = "2")
    private Integer type;

    /**
     * 开始有效时间
     */
    @ApiModelProperty(value = "开始有效时间",example = "2022-05-21")
    @JsonProperty("begin_time")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss",locale = "zh",timezone = "GMT+8")
    private LocalDateTime beginTime;

    /**
     * 有效结束时间
     */
    @ApiModelProperty(value = "有效结束时间",example = "2022-05-22")
    @JsonProperty("end_time")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss",locale = "zh",timezone = "GMT+8")
    private LocalDateTime endTime;


    /**
     * 是否已发布
     */
    @ApiModelProperty(value = "是否已发布",example = "0")
    @JsonProperty("is_publish")
    private Integer isPublish;

}
