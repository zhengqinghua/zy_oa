package com.hnxxxy.oa.information.service;


import com.hnxxxy.oa.information.request.MessageTypePageRequest;
import com.hnxxxy.oa.information.request.MessageTypeRequest;
import com.hnxxxy.oa.information.vo.MessageTypeVO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 消息类型表 服务类
 * </p>
 *
 * @author zqh
 * @since 2022-04-24
 */
public interface MessageTypeService {

    /**
     * 新增消息类型
     * @param messageTypeRequest
     */
    void add(MessageTypeRequest messageTypeRequest);

    /**
     * 删除消息类型
     * @param id
     */
    void delete(int id);

    /**
     * 修改消息类型
     * @param id
     * @param messageTypeRequest
     */
    // void update(int id, MessageTypeRequest messageTypeRequest);
    void update(MessageTypeRequest messageTypeRequest);

    /**
     * 查询全部消息类型
     * @return
     */
    List<MessageTypeVO> findAllMessageType();

    /**
     * 分页查询消息类型列表
     * @param messageTypePageRequest
     * @return
     */
    Map<String, Object> page(MessageTypePageRequest messageTypePageRequest);


    /**
     * 根据id查询消息类型
     * @param id
     * @return
     */
    MessageTypeVO findByIdMessageType(int id);
}
