package com.hnxxxy.oa.information.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 消息发送对象
 * </p>
 *
 * @author zqh
 * @since 2022-04-24
 */
@ApiModel(value = "消息发送请求",description = "消息发送请求对象")
@Data
public class MessageToUserRequest {

    /**
     * 消息Id
     */
    @ApiModelProperty(value = "消息id",example = "5")
    @JsonProperty("message_id")
    private Integer messageId;

    /**
     * 发送对象Id
     */
    @ApiModelProperty(value = "发送对象id",example = "3")
    @JsonProperty("to_user_id")
    private Integer toUserId;

}
