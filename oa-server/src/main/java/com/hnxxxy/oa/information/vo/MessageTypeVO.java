package com.hnxxxy.oa.information.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * <p>
 * 消息类型表
 * </p>
 *
 * @author zqh
 * @since 2022-04-24
 */
@Data
public class MessageTypeVO {

    private static final long serialVersionUID = 1L;

    /**
     * 消息类型Id
     */
    private Integer id;

    /**
     * 消息类型名称
     */
    private String name;

    /**
     * 消息类型描述
     */
    private String bewrite;

}
