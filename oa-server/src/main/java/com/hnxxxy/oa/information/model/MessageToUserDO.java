package com.hnxxxy.oa.information.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 消息发送对象
 * </p>
 *
 * @author zqh
 * @since 2022-04-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("message_to_user")
public class MessageToUserDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 序号Id
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 消息Id
     */
    private Integer messageId;

    /**
     * 发送对象Id
     */
    private Integer toUserId;

    /**
     * 是否已读。1：已读、0：未读
     */
    private Integer isRead;

    /**
     * 创建日期
     */
    private LocalDateTime createTime;


}
