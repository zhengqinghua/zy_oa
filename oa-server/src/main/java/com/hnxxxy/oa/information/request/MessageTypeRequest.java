package com.hnxxxy.oa.information.request;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 消息类型表
 * </p>
 *
 * @author zqh
 * @since 2022-04-24
 */
@ApiModel(value = "消息类型请求",description = "消息类型请求对象")
@Data
public class MessageTypeRequest {


    /**
     * 消息类型Id
     */
    private Integer id;

    /**
     * 消息类型名称
     */
    @ApiModelProperty(value = "消息类型名称",example = "java")
    private String name;

    /**
     * 消息类型描述
     */
    @ApiModelProperty(value = "消息类型描述",example = "java66666")
    private String bewrite;

}
