package com.hnxxxy.oa.information.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户已读消息表
 * </p>
 *
 * @author zqh
 * @since 2022-04-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("read_common_message")
public class ReadCommonMessageDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 序号Id
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 消息Id
     */
    private Integer messageId;

    /**
     * 消息读取者
     */
    private Integer userId;

    /**
     * 创建日期
     */
    private LocalDateTime createTime;


}
