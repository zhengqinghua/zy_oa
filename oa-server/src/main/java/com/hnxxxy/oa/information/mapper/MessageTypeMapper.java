package com.hnxxxy.oa.information.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hnxxxy.oa.information.model.MessageTypeDO;

/**
 * <p>
 * 消息类型表 Mapper 接口
 * </p>
 *
 * @author zqh
 * @since 2022-04-24
 */
public interface MessageTypeMapper extends BaseMapper<MessageTypeDO> {

}
