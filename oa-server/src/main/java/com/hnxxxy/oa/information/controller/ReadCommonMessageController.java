package com.hnxxxy.oa.information.controller;


import com.hnxxxy.oa.information.service.MessageService;
import com.hnxxxy.oa.information.service.ReadCommonMessageService;
import com.hnxxxy.oa.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用户已读消息表 前端控制器
 * </p>
 *
 * @author zqh
 * @since 2022-04-24
 */
@RestController
@RequestMapping("/api/readcommonmessage/v1")
@Api(value = "ReadCommonMessageController", tags = {"消息已读模块"})
public class ReadCommonMessageController {

    @Autowired
    private MessageService messageService;

    @Autowired
    private ReadCommonMessageService readCommonMessageService;

    /**
     * 消息已读
     * @param messageId
     * @return
     */
    @PostMapping("/confirm/{message_id}")
    @ApiOperation(value = "消息确认")
    public R confirm(@ApiParam(value = "消息id",required = true)
                     @PathVariable("message_id") int messageId){

        readCommonMessageService.confirm(messageId);
        return R.buildSuccess();
    }
}

