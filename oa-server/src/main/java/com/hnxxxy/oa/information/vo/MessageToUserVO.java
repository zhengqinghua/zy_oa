package com.hnxxxy.oa.information.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 * 消息发送对象
 * </p>
 *
 * @author zqh
 * @since 2022-04-24
 */
@Data
public class MessageToUserVO {

    /**
     * 序号Id
     */
    private Integer id;

    /**
     * 消息Id
     */
    @JsonProperty("message_id")
    private Integer messageId;

    /**
     * 发送对象Id
     */
    @JsonProperty("to_user_id")
    private Integer toUserId;

    /**
     * 是否已读。1：已读、0：未读
     */
    @JsonProperty("is_read")
    private Integer isRead;

    /**
     * 创建日期
     */
    // @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss",locale = "zh",timezone = "GMT+8")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd",locale = "zh",timezone = "GMT+8")
    @JsonProperty("create_time")
    private LocalDateTime createTime;


}
