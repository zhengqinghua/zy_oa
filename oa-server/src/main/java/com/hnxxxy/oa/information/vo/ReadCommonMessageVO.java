package com.hnxxxy.oa.information.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 * 用户已读消息表
 * </p>
 *
 * @author zqh
 * @since 2022-04-24
 */
@Data
public class ReadCommonMessageVO {

    /**
     * 序号Id
     */
    private Integer id;

    /**
     * 消息Id
     */
    @JsonProperty("message_id")
    private Integer messageId;

    /**
     * 消息读取者
     */
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 创建日期
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss",locale = "zh",timezone = "GMT+8")
    @JsonProperty("create_time")
    private LocalDateTime createTime;


}
