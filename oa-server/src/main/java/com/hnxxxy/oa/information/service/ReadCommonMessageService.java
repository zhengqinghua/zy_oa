package com.hnxxxy.oa.information.service;

/**
 * <p>
 * 用户已读消息表 服务类
 * </p>
 *
 * @author zqh
 * @since 2022-04-24
 */
public interface ReadCommonMessageService {

    /**
     * 消息已读
     * @param messageId
     */
    void confirm(int messageId);
}
