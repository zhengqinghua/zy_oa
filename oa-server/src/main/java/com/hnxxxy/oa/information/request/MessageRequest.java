package com.hnxxxy.oa.information.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 * 消息表
 * </p>
 *
 * @author zqh
 * @since 2022-04-24
 */
@ApiModel(value = "消息请求",description = "消息请求对象")
@Data
public class MessageRequest {

    /**
     * 消息标题
     */
    @ApiModelProperty(value = "标题",example = "Java入门")
    private String title;

    /**
     * 消息内容
     */
    @ApiModelProperty(value = "内容",example = "Java888888888888888888888")
    private String content;

    /**
     * 消息类型
     */
    @ApiModelProperty(value = "类型",example = "2")
    private Integer type;

    /**
     * 开始有效时间
     */
    @ApiModelProperty(value = "开始有效时间",example = "2020-03-01 14:49:44")
    @JsonProperty("begin_time")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss",locale = "zh",timezone = "GMT+8")
    private LocalDateTime beginTime;

    /**
     * 有效结束时间
     */
    @ApiModelProperty(value = "有效结束时间",example = "2020-03-02 14:49:44")
    @JsonProperty("end_time")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss",locale = "zh",timezone = "GMT+8")
    private LocalDateTime endTime;

    /**
     * 消息发送的对象
     */
    @ApiModelProperty(value = "是否已发布",example = "0")
    private Integer[] userList;

    /**
     * 是否已发布
     */
    @ApiModelProperty(value = "是否已发布",example = "0")
    @JsonProperty("is_publish")
    private Integer isPublish;

}
