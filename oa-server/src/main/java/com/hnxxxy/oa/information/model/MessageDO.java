package com.hnxxxy.oa.information.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 消息表
 * </p>
 *
 * @author zqh
 * @since 2022-04-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("message")
public class MessageDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 消息Id
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 消息标题
     */
    private String title;

    /**
     * 消息内容
     */
    private String content;

    /**
     * 消息类型
     */
    private Integer type;

    /**
     * 开始有效时间
     */
    private LocalDateTime beginTime;

    /**
     * 有效结束时间
     */
    private LocalDateTime endTime;

    /**
     * 发送者
     */
    @TableField("from_userId")
    private Integer fromUserid;

    /**
     * 是否已发布
     */
    private Integer isPublish;

    /**
     * 发送时间
     */
    private LocalDateTime recordTime;

    /**
     * 创建日期
     */
    private LocalDateTime createTime;


}
