package com.hnxxxy.oa.information.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hnxxxy.oa.information.mapper.MessageMapper;
import com.hnxxxy.oa.information.model.MessageDO;
import com.hnxxxy.oa.information.request.MessagePageRequest;
import com.hnxxxy.oa.information.request.MessageRequest;
import com.hnxxxy.oa.information.service.MessageService;
import com.hnxxxy.oa.information.vo.MessageVO;
import com.hnxxxy.oa.system.constant.UserConstant;
import com.hnxxxy.oa.system.model.SysUserDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @description: 消息模块
 * @author: zqh
 * @date: 2022/4/25 11:47
 **/
@Slf4j
@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageMapper messageMapper;

    /**
     * 新增消息
     * @param messageRequest
     */
    @Override
    public void add(MessageRequest messageRequest) {
            SysUserDO user = (SysUserDO) StpUtil.getSession().get(UserConstant.SESSION_USER_KEY);

            MessageDO messageDO = new MessageDO();
            messageDO.setCreateTime( LocalDateTime.now());
            messageDO.setFromUserid(user.getId());
            BeanUtils.copyProperties(messageRequest, messageDO);

            int rows = messageMapper.insert(messageDO);


            log.info("新增消息:rows={},data={}",rows,messageDO);
    }

    /**
     * 删除消息
     * @param id
     */
    @Override
    public void delete(int id) {
        SysUserDO user = (SysUserDO) StpUtil.getSession().get("userDO");
        int rows = messageMapper.delete(new QueryWrapper<MessageDO>().eq("id", id).eq("from_userId", user.getId()));
        log.info("删除消息:rows={}",rows);

    }

    /**
     * 修改消息
     * @param messageRequest
     */
    @Override
    public void update(int id, MessageRequest messageRequest) {
        SysUserDO user = (SysUserDO) StpUtil.getSession().get(UserConstant.SESSION_USER_KEY);
        MessageDO messageDO = new MessageDO();
        BeanUtils.copyProperties(messageRequest, messageDO);
        int rows = messageMapper.update(messageDO, new QueryWrapper<MessageDO>().eq("id", id).eq("from_userId", user.getId()));
        log.info("修改消息:rows={}",rows);
    }

    /**
     * 根据id查询消息
     * @return
     */
    @Override
    public MessageVO findByIdMessage(int id) {
        SysUserDO user = (SysUserDO) StpUtil.getSession().get(UserConstant.SESSION_USER_KEY);
        MessageVO messageVO = new MessageVO();
        MessageDO messageDO = messageMapper.selectOne(new QueryWrapper<MessageDO>().eq("id", id).eq("from_userId", user.getId()));

        BeanUtils.copyProperties(messageDO, messageVO);
        log.info("根据id查询信息:id={}  messageVO={}",id,messageVO);
        return messageVO;
    }

    /**
     * 查询全部消息
     * @return
     */
    @Override
    public List<MessageVO> findAllMessage() {
        SysUserDO user = (SysUserDO) StpUtil.getSession().get(UserConstant.SESSION_USER_KEY);
        List<MessageDO> messageDOList = messageMapper.selectList(new QueryWrapper<MessageDO>().eq("from_userId", user.getId()));

        List<MessageVO> messageVOList = messageDOList.stream().map(obj -> {
            MessageVO messageVO = new MessageVO();
            BeanUtils.copyProperties(obj, messageVO);
            return messageVO;
        }).collect(Collectors.toList());

        log.info("全部消息:messageVOList={}",messageVOList);
        return messageVOList;
    }

    /**
     * 分页查询消息
     * @param messagePageRequest
     * @return
     */
    @Override
    public Map<String, Object> page(MessagePageRequest messagePageRequest) {
        int page = messagePageRequest.getPage();
        int size = messagePageRequest.getSize();

        SysUserDO user = (SysUserDO) StpUtil.getSession().get(UserConstant.SESSION_USER_KEY);
        LocalDateTime beginTime = messagePageRequest.getBeginTime();
        LocalDateTime endTime = messagePageRequest.getEndTime();
        Integer isPublish = messagePageRequest.getIsPublish();
        String title = messagePageRequest.getTitle();

        QueryWrapper<MessageDO> queryWrapper = new QueryWrapper<MessageDO>().eq("from_userId",user.getId());

        if (Objects.nonNull(beginTime)) {
            queryWrapper.ge("begin_time", beginTime);
        }

        if (Objects.nonNull(endTime)) {
            queryWrapper.le("end_time", endTime);
        }

        if (isPublish != null){
            queryWrapper.eq("is_publish", isPublish);
        }

        if (!StrUtil.hasEmpty(title)){
            queryWrapper.like("title", title);
        }

        Page<MessageDO> pageInfo = new Page<>(page, size);

        IPage<MessageDO> messageDOIPage = messageMapper.selectPage(pageInfo,queryWrapper);

        Map<String,Object> pageMap = new HashMap<>(3);

        pageMap.put("total_record", messageDOIPage.getTotal());
        pageMap.put("total_page", messageDOIPage.getPages());
        pageMap.put("current_data",messageDOIPage.getRecords()
                .stream().map(obj->beanProocess(obj)).collect(Collectors.toList()));

        log.info("分页消息:pageMap={}",pageMap);
        return pageMap;
    }


    private MessageVO beanProocess(MessageDO obj) {
        MessageVO messageVO = new MessageVO();
        BeanUtils.copyProperties(obj, messageVO);
        return messageVO;
    }


}
