package com.hnxxxy.oa.system.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author ZHOUSHILIN
 * @data 2022 - 05 - 05 - 15:41
 */
@ToString
@Data
@ApiModel(value = "DistributeRoleRequest",description = "授权角色请求对象")
public class DistributeRoleRequest implements Serializable {

    private static final long serialVersionUID = 7432473445226990815L;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id")
    private Integer userId;

    /**
     * 角色id
     */
    @ApiModelProperty(value = "角色id")
    private Integer roleId;
}