package com.hnxxxy.oa.system.VO;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author ZHOUSHILIN
 * @data 2022 - 05 - 15 - 22:17
 */
@Data
public class SysLoginLogVO implements Serializable {


    private static final long serialVersionUID = 4541005195772085125L;

    /**
     * 登录日志Id
     */
    private Integer id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 登录时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @JsonProperty("time")
    private LocalDateTime time;

    /**
     * 登录是否成功(1：成功、0失败)
     */
    private Integer isSuccess;

    /**
     * IP地址
     */
    private String ip;

    /**
     * 登录备注
     */
    private String remark;

    /**
     * 登录时返回数据
     */
    private String message;
}
