package com.hnxxxy.oa.system.service;

import com.hnxxxy.oa.system.model.SysUserDO;
import com.hnxxxy.oa.system.request.GeneralPageRequest;
import com.hnxxxy.oa.system.request.UserPageByUsernameRequest;
import com.hnxxxy.oa.system.request.UserRegisterRequest;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;
import java.util.Map;

/**
* @author 周石林
* @description 针对表【sys_user(用户)】的数据库操作Service
* @createDate 2022-05-03 15:33:13
*/
public interface SysUserService{
    /**
     * 用户注册
     * @param userRegisterRequest
     * @return long
     */
    Integer userRegister(UserRegisterRequest userRegisterRequest);

    /**
     * 用户登录
     * @param username
     * @param password
     * @return SysUserDO
     */
    SysUserDO userLogin(String username, String password);

    /**
     * 脱敏
     * @param originUser
     * @return SysUserDO
     */
    SysUserDO getSafetyUser(SysUserDO originUser);

    /**
     * 删除用户
     * @param id
     * @return
     */
    boolean deleteUser(int id);

    /**
     * 列出所有用户
     * @return
     */
    List<SysUserDO> listUser();

    /**
     * 分页查询所有用户
     * @param userPageByUsernameRequest
     * @return
     */
    Map<String, Object> pageListUsers(UserPageByUsernameRequest userPageByUsernameRequest);

    /**
     * 根据用户Id查用户
     */
    SysUserDO selectUserById(int userId);

    /**
     * 批量删除用户
     */
    boolean deleteUserByBatch(String ids);
}
