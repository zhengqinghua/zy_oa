package com.hnxxxy.oa.system.mapper;

import com.hnxxxy.oa.system.model.SysRoleDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 周石林
* @description 针对表【sys_role(角色)】的数据库操作Mapper
* @createDate 2022-05-07 08:52:50
* @Entity com.hnxxxy.oa.system.model.SysRoleDO
*/
public interface SysRoleMapper extends BaseMapper<SysRoleDO> {

}




