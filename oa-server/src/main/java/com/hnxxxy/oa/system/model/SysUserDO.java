package com.hnxxxy.oa.system.model;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import lombok.Data;

/**
 * 用户
 * @TableName sys_user
 */
@TableName(value ="sys_user")
@Data
public class SysUserDO implements Serializable {

    /**
     * 用户唯一标识
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 实际名字
     */
    private String actualName;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 所在部门
     */
    private Integer departId;

    /**
     * 性别(1男 0女)
     */
    private Integer gender;

    /**
     * 状态（1正常 0屏蔽）
     */
    @TableLogic
    private Integer state;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}