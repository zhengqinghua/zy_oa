package com.hnxxxy.oa.system.mapper;

import com.hnxxxy.oa.system.model.SysUserRoleDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 周石林
* @description 针对表【sys_user_role(用户-角色)】的数据库操作Mapper
* @createDate 2022-05-05 16:06:48
* @Entity com.hnxxxy.oa.system.model.SysUserRoleDO
*/
public interface SysUserRoleMapper extends BaseMapper<SysUserRoleDO> {

}




