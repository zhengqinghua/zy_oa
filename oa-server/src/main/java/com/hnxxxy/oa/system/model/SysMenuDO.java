package com.hnxxxy.oa.system.model;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;

/**
 * 菜单
 * @TableName sys_menu
 */
@TableName(value ="sys_menu")
@Data
public class SysMenuDO implements Serializable {
    /**
     * 菜单节点id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 父节点id(一级菜单为0)
     */
    private Integer parentId;

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 菜单链接地址
     */
    private String url;

    /**
     * 菜单显示顺序
     */
    private Integer menuOrder;

    /**
     * 菜单类型（0：目录、1：菜单、2：按钮）
     */
    private Integer type;

    /**
     * 授权（多个用,分隔，如user:list,user:create）
     */
    private String permission;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 菜单状态（1、正常，0、禁用）
     */
    @TableLogic
    private Integer menuStatus;

    /**
     * 子菜单
     */
    @TableField(exist = false)
    private List<SysMenuDO> childs;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}