package com.hnxxxy.oa.system.request;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author ZHOUSHILIN
 * @data 2022 - 05 - 04 - 8:27
 */
@ToString
@Data
@ApiModel(value = "UserLoginRequest",description = "用户登录对象")
public class UserLoginRequest implements Serializable {

    private static final long serialVersionUID = -6987000778539974332L;

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    private String username;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;

}