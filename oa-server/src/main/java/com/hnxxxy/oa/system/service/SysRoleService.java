package com.hnxxxy.oa.system.service;

import com.hnxxxy.oa.system.model.SysRoleDO;
import com.hnxxxy.oa.system.request.RoleAddRequest;
import com.hnxxxy.oa.system.request.RolePageByRoleNameRequest;
import com.hnxxxy.oa.system.request.RoleUpdateRequest;
import com.hnxxxy.oa.utils.R;
import io.swagger.models.auth.In;

import java.util.List;
import java.util.Map;

/**
* @author 周石林
* @description 针对表【sys_role(角色)】的数据库操作Service
* @createDate 2022-05-03 15:33:13
*/
public interface SysRoleService{

    Integer roleAdd(RoleAddRequest roleAddRequest);

    Integer roleUpdate(RoleUpdateRequest roleUpdateRequest);

    boolean roleDelete(Integer id);

    Map<String, Object> pageListRole(RolePageByRoleNameRequest rolePageByRoleNameRequest);
}