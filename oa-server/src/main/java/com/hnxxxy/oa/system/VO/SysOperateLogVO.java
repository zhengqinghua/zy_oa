package com.hnxxxy.oa.system.VO;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author ZHOUSHILIN
 * @data 2022 - 05 - 15 - 21:16
 */
@Data
public class SysOperateLogVO implements Serializable {

    private static final long serialVersionUID = 6697925598732196304L;

    /**
     * 操作日志Id
     */
    private Integer id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 业务模块名
     */
    private String businessName;

    /**
     * 方法名
     */
    private String methodName;

    /**
     * 请求参数
     */
    private String httpParam;

    /**
     * IP地址
     */
    private String ip;

    /**
     * 操作状态（1、正常，0、异常）
     */
    private Integer logStatus;

    /**
     * 返回结果
     */
    private String result;

    /**
     * 错误信息
     */
    private String error;

    /**
     * 操作备注
     */
    private String remark;

    /**
     * 操作时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @JsonProperty("operateTime")
    private LocalDateTime operateTime;

}