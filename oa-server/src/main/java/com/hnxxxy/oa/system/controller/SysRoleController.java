package com.hnxxxy.oa.system.controller;

import com.hnxxxy.oa.annotation.SysLog;
import com.hnxxxy.oa.enums.BizCodeEnum;
import com.hnxxxy.oa.system.model.SysRoleDO;
import com.hnxxxy.oa.system.request.RoleAddRequest;
import com.hnxxxy.oa.system.request.RolePageByRoleNameRequest;
import com.hnxxxy.oa.system.request.RoleUpdateRequest;
import com.hnxxxy.oa.system.service.SysRoleMenuService;
import com.hnxxxy.oa.system.service.SysRoleService;
import com.hnxxxy.oa.system.service.SysUserRoleService;
import com.hnxxxy.oa.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author ZHOUSHILIN
 * @data 2022 - 05 - 05 - 10:15
 */
@RestController
@RequestMapping("/api/system/v1")
@Api(value = "SysRoleController",tags = "角色接口")
public class SysRoleController {

    @Resource
    private SysRoleService sysRoleService;

    @Resource
    private SysUserRoleService sysUserRoleService;

    @Resource
    private SysRoleMenuService sysRoleMenuService;

    @ApiOperation(value = "添加角色")
    @PostMapping("/addRole")
    @SysLog("添加角色")
    public R addRole(@RequestBody RoleAddRequest roleAddRequest){
        if(roleAddRequest==null){
            return R.buildResult(BizCodeEnum.PARAM_ERROR);
        }
        String roleName = roleAddRequest.getRoleName();
        String roleKey = roleAddRequest.getRoleKey();
        if(StringUtils.isAnyBlank(roleName,roleKey)){
            return R.buildResult(BizCodeEnum.PARAM_NULL);
        }
        long roleId = sysRoleService.roleAdd(roleAddRequest);
        return R.buildSuccess(roleId);
    }

    @ApiOperation(value = "修改角色")
    @PostMapping("/updateRole")
    @SysLog("修改角色")
    public R updateRole(@RequestBody RoleUpdateRequest roleUpdateRequest){
        if(roleUpdateRequest==null){
            return R.buildResult(BizCodeEnum.PARAM_ERROR);
        }
        Integer id = roleUpdateRequest.getId();
        String roleKey = roleUpdateRequest.getRoleKey();
        String roleName = roleUpdateRequest.getRoleName();
        if(id==null){
            return R.buildResult(BizCodeEnum.PARAM_NULL);
        }
        if(StringUtils.isAnyBlank(roleKey,roleName)){
            return R.buildResult(BizCodeEnum.PARAM_NULL);
        }
        long roleId = sysRoleService.roleUpdate(roleUpdateRequest);
        return R.buildSuccess(roleId);
    }

    @ApiOperation(value = "删除角色")
    @PostMapping("/deleteRole")
    @SysLog("删除角色")
    public R deleteRole(@ApiParam(value = "角色id",required = true)
                                    @RequestBody Integer id){
        //删除角色
        boolean b = sysRoleService.roleDelete(id);
        //删除有该角色的用户角色
        sysUserRoleService.deleteUserRole(null, id);
        //删除该角色授权
        sysRoleMenuService.deleteGrantAuthorzation(id,null);
        return R.buildSuccess(b);
    }

    @ApiOperation(value = "分页展示所有角色")
    @PostMapping("/pageListRole")
    @SysLog("分页展示所有角色")
    public R pageListRole(@RequestBody RolePageByRoleNameRequest rolePageByRoleNameRequest){
        if (rolePageByRoleNameRequest==null){
            return R.buildResult(BizCodeEnum.PARAM_NULL);
        }
        Map<String, Object> map=sysRoleService.pageListRole(rolePageByRoleNameRequest);
        return R.buildSuccess(map);
    }
}