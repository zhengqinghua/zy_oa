package com.hnxxxy.oa.system.constant;

/**
 * @author ZHOUSHILIN
 * @data 2022 - 05 - 04 - 9:07
 */
public interface UserConstant {

    /**
     * 用户登录信息键
     */
    String SESSION_USER_KEY = "UserInfo";

}
