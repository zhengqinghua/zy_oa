package com.hnxxxy.oa.system.service;

/**
* @author 周石林
* @description 针对表【sys_user_role(用户-角色)】的数据库操作Service
* @createDate 2022-05-05 16:06:48
*/
public interface SysUserRoleService{
    /**
     * 授权角色
     * @param userId
     * @param roleId
     * @return
     */
    Integer addUserRole(Integer userId, Integer roleId);

    boolean deleteUserRole(Integer userId, Integer roleId);
}