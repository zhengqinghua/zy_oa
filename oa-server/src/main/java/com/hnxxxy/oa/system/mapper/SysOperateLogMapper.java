package com.hnxxxy.oa.system.mapper;

import com.hnxxxy.oa.system.model.SysOperateLogDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 周石林
* @description 针对表【sys_operate_log(操作日志表)】的数据库操作Mapper
* @createDate 2022-05-15 18:58:28
* @Entity com.hnxxxy.oa.system.model.SysOperateLogDO
*/
public interface SysOperateLogMapper extends BaseMapper<SysOperateLogDO> {

}




