package com.hnxxxy.oa.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hnxxxy.oa.annotation.SysLog;
import com.hnxxxy.oa.enums.BizCodeEnum;
import com.hnxxxy.oa.system.model.SysLoginLogDO;
import com.hnxxxy.oa.system.request.PageListToLoginLogRequest;
import com.hnxxxy.oa.system.request.PageListToOperateLogRequest;
import com.hnxxxy.oa.system.service.SysLoginLogService;
import com.hnxxxy.oa.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author ZHOUSHILIN
 * @data 2022 - 05 - 15 - 19:15
 */
@Api(value = "SysLoginLogController",tags = "用户登录日志接口")
@RestController
@RequestMapping("/api/system/v1")
public class SysLoginLogController {

    @Resource
    private SysLoginLogService sysLoginLogService;

    @PostMapping("/pageListMutipleConditionsToLoginLog")
    @ApiOperation("多条件分页查询登录日志")
    @SysLog("多条件分页查询登录日志")
    public R pageListByUsername(
            @ApiParam("多条件分页查询操作日志对象")
            @RequestBody PageListToLoginLogRequest pageListToLoginLogRequest){
        if (pageListToLoginLogRequest==null){
            return R.buildResult(BizCodeEnum.PARAM_ERROR);
        }
        Map<String, Object> stringObjectMap = sysLoginLogService.pageListToLoginLog(pageListToLoginLogRequest);
        return R.buildSuccess(stringObjectMap);
    }

}