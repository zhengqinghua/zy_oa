package com.hnxxxy.oa.system.VO;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author ZHOUSHILIN
 * @data 2022 - 05 - 15 - 22:04
 */
@Data
public class SysUserVO implements Serializable{

    private static final long serialVersionUID = 7580457907216853942L;

    /**
     * 用户id
     */
    private Integer id;

    /**
     * 实际名字
     */
    private String actualName;

    /**
     * 用户名
     */
    private String username;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 所在部门
     */
    private Integer departId;

    /**
     * 性别(1男 0女)
     */
    private Integer gender;

    /**
     * 状态（1正常 0屏蔽）
     */
    private Integer state;


    /**
     * 创建时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @JsonProperty("createTime")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @JsonProperty("updateTime")
    private LocalDateTime updateTime;
}
