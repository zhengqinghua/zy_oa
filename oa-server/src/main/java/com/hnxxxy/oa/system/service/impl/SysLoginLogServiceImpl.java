package com.hnxxxy.oa.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hnxxxy.oa.system.VO.SysLoginLogVO;
import com.hnxxxy.oa.system.VO.SysOperateLogVO;
import com.hnxxxy.oa.system.mapper.SysLoginLogMapper;
import com.hnxxxy.oa.system.model.SysLoginLogDO;
import com.hnxxxy.oa.system.model.SysOperateLogDO;
import com.hnxxxy.oa.system.request.PageListToLoginLogRequest;
import com.hnxxxy.oa.system.service.SysLoginLogService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
* @author 周石林
* @description 针对表【sys_login_log(登录日志表)】的数据库操作Service实现
* @createDate 2022-05-03 15:33:12
*/
@Service
public class SysLoginLogServiceImpl implements SysLoginLogService{

    @Resource
    private SysLoginLogMapper sysLoginLogMapper;

    @Override
    public void insert(SysLoginLogDO sysLoginLogDO){
        sysLoginLogMapper.insert(sysLoginLogDO);
    }

    @Override
    public String getIpAddr(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

    @Override
    public Map<String, Object> pageListToLoginLog(PageListToLoginLogRequest pageListToLoginLogRequest) {
        LocalDateTime time = pageListToLoginLogRequest.getTime();
        String username = pageListToLoginLogRequest.getUsername();
        Integer page = pageListToLoginLogRequest.getPage();
        Integer size = pageListToLoginLogRequest.getSize();
        if (page<=0){
            page=1;
        }
        if (size<=0){
            size=10;
        }
        QueryWrapper<SysLoginLogDO> queryWrapper = new QueryWrapper<>();
        if (time!=null){
            //转换时间格式
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String format = dateTimeFormatter.format(time);
            queryWrapper.like("time",format+"%");
        }
        queryWrapper.like(!Strings.isBlank(username),"username","%"+username+"%");
        Page<SysLoginLogDO> pageInfo = new Page<>(page,size);
        Page<SysLoginLogDO> sysOperateLogDOPage = sysLoginLogMapper.selectPage(pageInfo, queryWrapper);
        Map<String,Object> pageMap = new HashMap<>(3);
        pageMap.put("total_record",sysOperateLogDOPage.getTotal());
        pageMap.put("total_page",sysOperateLogDOPage.getPages());
        pageMap.put("current_data",sysOperateLogDOPage.getRecords().stream()
                .map(obj->beanProocess(obj)).collect(Collectors.toList()));
        return pageMap;
    }

    private SysLoginLogVO beanProocess(SysLoginLogDO obj) {
        SysLoginLogVO loginLogVO = new SysLoginLogVO();
        BeanUtils.copyProperties(obj,loginLogVO);
        return loginLogVO;
    }
}