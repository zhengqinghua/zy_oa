package com.hnxxxy.oa.system.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author ZHOUSHILIN
 * @data 2022 - 05 - 22 - 11:13
 */
@ToString
@Data
public class RolePageByRoleNameRequest implements Serializable {

    private static final long serialVersionUID = -5428878189188635841L;

    @ApiModelProperty("当前页")
    public Integer page;

    @ApiModelProperty("每页显示多少条数据")
    public Integer size;

    @ApiModelProperty("根据角色名模糊查询")
    public String roleName;

}
