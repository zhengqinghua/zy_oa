package com.hnxxxy.oa.system.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author ZHOUSHILIN
 * @data 2022 - 05 - 05 - 22:47
 */
@ToString
@Data
@ApiModel(value = "MenuAddRequest",description = "添加菜单请求对象")
public class MenuAddRequest implements Serializable {

    private static final long serialVersionUID = -6211727357672652180L;

    /**
     * 父节点id
     */
    @ApiModelProperty("父节点id")
    private Integer parentId;

    /**
     * 菜单名称
     */
    @ApiModelProperty("菜单名称")
    private String menuName;

    /**
     * 菜单链接地址
     */
    @ApiModelProperty("菜单链接地址")
    private String url;

    /**
     * 菜单显示顺序
     */
    @ApiModelProperty("菜单显示顺序")
    private Integer menuOrder;

    /**
     * 授权（多个用,分隔，如user:list,user:create）
     */
    @ApiModelProperty("授权标识")
    private String permission;

    /**
     * 菜单类型（0：目录、1：菜单、2：按钮）
     */
    @ApiModelProperty("菜单类型（0：目录、1：菜单、2：按钮）")
    private Integer type;
}
