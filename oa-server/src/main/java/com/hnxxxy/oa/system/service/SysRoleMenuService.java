package com.hnxxxy.oa.system.service;

/**
* @author 周石林
* @description 针对表【sys_role_menu(角色-菜单)】的数据库操作Service
* @createDate 2022-05-03 15:33:13
*/
public interface SysRoleMenuService{

    Integer grantAuthorzation(Integer[] integers);

    boolean deleteGrantAuthorzation(Integer roleId, Integer menuId);
}