package com.hnxxxy.oa.system.mapper;

import com.hnxxxy.oa.system.model.SysMenuDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 周石林
* @description 针对表【sys_menu(菜单)】的数据库操作Mapper
* @createDate 2022-05-07 15:39:34
* @Entity com.hnxxxy.oa.system.model.SysMenuDO
*/
public interface
SysMenuMapper extends BaseMapper<SysMenuDO> {

}




