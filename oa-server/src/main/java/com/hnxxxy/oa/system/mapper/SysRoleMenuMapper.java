package com.hnxxxy.oa.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hnxxxy.oa.system.model.SysRoleMenuDO;

/**
* @author 周石林
* @description 针对表【sys_role_menu(角色-菜单)】的数据库操作Mapper
* @createDate 2022-05-03 15:33:13
* @Entity com.hnxxxy.oa.system.DO.SysRoleMenuDO
*/
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenuDO> {

}




