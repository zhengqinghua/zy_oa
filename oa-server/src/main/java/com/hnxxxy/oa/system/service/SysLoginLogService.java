package com.hnxxxy.oa.system.service;


import cn.hutool.http.HttpRequest;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hnxxxy.oa.system.model.SysLoginLogDO;
import com.hnxxxy.oa.system.request.PageListToLoginLogRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
* @author 周石林
* @description 针对表【sys_login_log(登录日志表)】的数据库操作Service
* @createDate 2022-05-03 15:33:12
*/
public interface SysLoginLogService{

    /**
     * 插入登录日志
     * @param sysLoginLogDO
     */
    void insert(SysLoginLogDO sysLoginLogDO);

    /**
     * 获取ip地址
     * @param request
     * @return
     */
    String getIpAddr(HttpServletRequest request);


    Map<String, Object> pageListToLoginLog(PageListToLoginLogRequest pageListToLoginLogRequest);
}
