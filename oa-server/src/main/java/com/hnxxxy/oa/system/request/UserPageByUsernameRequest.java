package com.hnxxxy.oa.system.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author ZHOUSHILIN
 * @data 2022 - 05 - 21 - 15:16
 */
@Data
@ToString
@ApiModel(value = "UserPageByUsernameRequest",description = "用户名模糊查询分页请求对象")
public class UserPageByUsernameRequest implements Serializable {

    private static final long serialVersionUID = 554754067514718885L;

    @ApiModelProperty("当前页")
    public Integer page;

    @ApiModelProperty("每页显示多少条数据")
    public Integer size;

    @ApiModelProperty("根据用户名模糊查询")
    public String username;
}