package com.hnxxxy.oa.system.service;

import com.hnxxxy.oa.system.model.SysMenuDO;
import com.hnxxxy.oa.system.request.MenuAddRequest;
import com.hnxxxy.oa.system.request.MenuUpdateRequest;

import java.util.List;

/**
* @author 周石林
* @description 针对表【sys_menu(菜单)】的数据库操作Service
* @createDate 2022-05-03 15:33:12
*/
public interface SysMenuService{

    Integer addMenu(MenuAddRequest menuAddRequest);

    boolean updateMenu(MenuUpdateRequest menuUpdateRequest);

    boolean deleteMenu(Integer id);

    List<SysMenuDO> listWithTree();

    /**
     * 列出所有菜单
     * @return
     */
    List<SysMenuDO> list();

    List<SysMenuDO> getList();
}
