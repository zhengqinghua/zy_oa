package com.hnxxxy.oa.system.constant;

/**
 * @author ZHOUSHILIN
 * @data 2022 - 05 - 12 - 10:37
 */
public interface MenuType {
    int CATALOG=0;
    int MENU=1;
    int BUTTON=2;
}
