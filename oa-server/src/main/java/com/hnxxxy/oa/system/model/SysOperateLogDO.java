package com.hnxxxy.oa.system.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import lombok.Data;
import lombok.ToString;

/**
 * 操作日志表
 * @TableName sys_operate_log
 */
@ToString
@TableName(value ="sys_operate_log")
@Data
public class SysOperateLogDO implements Serializable {
    /**
     * 操作日志Id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 操作者
     */
    private Integer operateId;

    /**
     * 用户名
     */
    private String username;

    /**
     * 业务模块名
     */
    private String businessName;

    /**
     * 方法名
     */
    private String methodName;

    /**
     * 请求参数
     */
    private String httpParam;

    /**
     * IP地址
     */
    private String ip;

    /**
     * 操作状态（1、正常，0、异常）
     */
    private Integer logStatus;

    /**
     * 返回结果
     */
    private String result;

    /**
     * 错误信息
     */
    private String error;

    /**
     * 操作备注
     */
    private String remark;

    /**
     * 操作时间
     */
    private LocalDateTime operateTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}