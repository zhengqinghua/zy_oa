package com.hnxxxy.oa.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hnxxxy.oa.system.model.SysOperateLogDO;
import com.hnxxxy.oa.system.request.PageListToOperateLogRequest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;

/**
* @author 周石林
* @description 针对表【sys_operate_log(操作日志表)】的数据库操作Service
* @createDate 2022-05-03 15:33:13
*/
public interface SysOperateLogService{

    void insert(SysOperateLogDO sysLog);

    Map<String, Object> pageList(int page, int size);

    Map<String, Object> pageListToOperateLogRequest(PageListToOperateLogRequest pageListToOperateLogRequest);


}
