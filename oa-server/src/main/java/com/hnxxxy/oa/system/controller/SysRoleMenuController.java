package com.hnxxxy.oa.system.controller;

import com.hnxxxy.oa.annotation.SysLog;
import com.hnxxxy.oa.enums.BizCodeEnum;
import com.hnxxxy.oa.system.service.SysRoleMenuService;
import com.hnxxxy.oa.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author ZHOUSHILIN
 * @data 2022 - 05 - 13 - 8:53
 */
@Api(value = "SysRoleMenuController",tags = "角色菜单接口")
@RestController
@RequestMapping("/api/system/v1")
public class SysRoleMenuController {

    @Resource
    private SysRoleMenuService sysRoleMenuService;

    @PostMapping("/grantAuthorization")
    @ApiOperation(value = "授权")
    @SysLog("授权")
    public R grantAuthorization(@ApiParam(value = "角色id、菜单id",required = true)
                                    @RequestBody Integer[] integers){
        if (integers.length<=1){
            return R.buildResult(BizCodeEnum.PERMISSTIONS_NULL);
        }
        Integer integer = sysRoleMenuService.grantAuthorzation(integers);
        return R.buildSuccess(integer);
    }

    @PostMapping("/deleteGrantAuthorization")
    @ApiOperation(value = "取消授权")
    @SysLog("取消授权")
    public R deleteGrantAuthorization(@ApiParam(value = "角色id",required = true)
                                        @RequestParam("roleId")
                                                  Integer roleId,
                                      @ApiParam(value = "菜单id",required = true)
                                              @RequestParam("menuId")
                                              Integer menuId){
        if (roleId==null || menuId==null){
            return R.buildResult(BizCodeEnum.PARAM_ERROR);
        }
        boolean b = sysRoleMenuService.deleteGrantAuthorzation(roleId, menuId);
        if (b){
           return R.buildSuccess(b);
        }
        return R.buildResult(BizCodeEnum.ROLE_MENU_UNEXIST);
    }
}
