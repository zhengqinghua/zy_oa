package com.hnxxxy.oa.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hnxxxy.oa.enums.BizCodeEnum;
import com.hnxxxy.oa.exception.BizException;
import com.hnxxxy.oa.system.mapper.SysRoleMapper;
import com.hnxxxy.oa.system.mapper.SysUserRoleMapper;
import com.hnxxxy.oa.system.model.SysRoleDO;
import com.hnxxxy.oa.system.model.SysUserDO;
import com.hnxxxy.oa.system.request.RoleAddRequest;
import com.hnxxxy.oa.system.request.RolePageByRoleNameRequest;
import com.hnxxxy.oa.system.request.RoleUpdateRequest;
import com.hnxxxy.oa.system.service.SysRoleMenuService;
import com.hnxxxy.oa.system.service.SysRoleService;
import com.hnxxxy.oa.system.service.SysUserRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
* @author 周石林
* @description 针对表【sys_role(角色)】的数据库操作Service实现
* @createDate 2022-05-03 15:33:13
*/
@Service
public class SysRoleServiceImpl implements SysRoleService{

    @Resource
    private SysRoleMapper sysRoleMapper;

    @Override
    public Integer roleAdd(RoleAddRequest roleAddRequest) {
        SysRoleDO sysRoleDO = new SysRoleDO();
        sysRoleDO.setRoleName(roleAddRequest.getRoleName());
        sysRoleDO.setRoleKey(roleAddRequest.getRoleKey());

        QueryWrapper<SysRoleDO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("role_name",sysRoleDO.getRoleName());
        Integer count = sysRoleMapper.selectCount(queryWrapper);
        if(count>0){
            throw new BizException(BizCodeEnum.ROLE_REPEAT);
        }
        sysRoleMapper.insert(sysRoleDO);
        return sysRoleDO.getId();
    }

    @Override
    public Integer roleUpdate(RoleUpdateRequest roleUpdateRequest) {
        SysRoleDO sysRoleDO = sysRoleMapper.selectById(roleUpdateRequest.getId());
        if(sysRoleDO==null){
            throw new BizException(BizCodeEnum.ROLE_NOTEXISTS);
        }
        sysRoleDO.setRoleName(roleUpdateRequest.getRoleName());
        sysRoleDO.setRoleKey(roleUpdateRequest.getRoleKey());
        QueryWrapper<SysRoleDO>  queryWrapper= new QueryWrapper<>();
        queryWrapper.eq("role_name",roleUpdateRequest.getRoleName());
        if(sysRoleMapper.selectCount(queryWrapper)>0){
            throw new BizException(BizCodeEnum.ROLE_REPEAT);
        }
        sysRoleMapper.updateById(sysRoleDO);
        return sysRoleDO.getId();
    }

    @Override
    public boolean roleDelete(Integer id) {
        SysRoleDO sysRoleDO = sysRoleMapper.selectById(id);
        if(sysRoleDO==null){
            throw new BizException(BizCodeEnum.ROLE_NOTEXISTS);
        }
        int i = sysRoleMapper.deleteById(id);
        return i == 1;
    }

    @Override
    public Map<String, Object> pageListRole(RolePageByRoleNameRequest rolePageByRoleNameRequest) {
        Integer page = rolePageByRoleNameRequest.getPage();
        Integer size = rolePageByRoleNameRequest.getSize();
        String roleName = rolePageByRoleNameRequest.getRoleName();
        //page为空设置默认值
        if (page==null || page<=0){
            page=1;
        }
        //size为空设置默认值
        if (size==null || size <=0){
            size=10;
        }
        Page<SysRoleDO> pageInfo = new Page<>(page,size);
        QueryWrapper<SysRoleDO> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(roleName!=null,"role_name","%"+roleName+"%");
        Page<SysRoleDO> sysRoleDOPage = sysRoleMapper.selectPage(pageInfo, queryWrapper);
        Map<String,Object> pageMap = new HashMap<>(3);
        pageMap.put("total_record",sysRoleDOPage.getTotal());
        pageMap.put("total_page",sysRoleDOPage.getPages());
        pageMap.put("current_data",sysRoleDOPage.getRecords());
        return pageMap;
    }
}