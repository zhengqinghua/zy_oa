package com.hnxxxy.oa.system.request;

import com.sun.org.apache.xml.internal.serializer.utils.SerializerMessages_zh_CN;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author ZHOUSHILIN
 * @data 2022 - 05 - 20 - 14:40
 */
@Data
@ToString
@ApiModel(value = "GeneralPageRequest",description = "通用分页请求对象")
public class GeneralPageRequest implements Serializable {

    private static final long serialVersionUID = -152847770164156499L;

    @ApiModelProperty("当前页")
    public Integer page;

    @ApiModelProperty("每页显示多少条数据")
    public Integer size;


}
