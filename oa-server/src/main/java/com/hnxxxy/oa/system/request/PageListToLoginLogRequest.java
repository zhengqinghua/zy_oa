package com.hnxxxy.oa.system.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author ZHOUSHILIN
 * @data 2022 - 05 - 22 - 16:57
 */
@Data
@ToString
public class PageListToLoginLogRequest implements Serializable {
    private static final long serialVersionUID = 2667277240383139111L;

    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    private String username;

    /**
     * 操作时间
     */
    @ApiModelProperty("操作时间")
    @JsonProperty("time")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss",locale = "zh",timezone = "GMT+8")
    private LocalDateTime time;

    @ApiModelProperty("当前页")
    public Integer page;

    @ApiModelProperty("每页显示多少条数据")
    public Integer size;
}
