package com.hnxxxy.oa.system.controller;

import com.hnxxxy.oa.annotation.SysLog;
import com.hnxxxy.oa.enums.BizCodeEnum;
import com.hnxxxy.oa.system.request.PageListToOperateLogRequest;
import com.hnxxxy.oa.system.service.SysOperateLogService;
import com.hnxxxy.oa.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Map;

/**
 * @author ZHOUSHILIN
 * @data 2022 - 05 - 15 - 19:25
 */
@Api(value = "SysOperateLogController",tags = "操作日志接口")
@RestController
@RequestMapping("/api/system/v1")
public class SysOperateLogController {

    @Resource
    private SysOperateLogService sysOperateLogService;

    @PostMapping("/pageListOperateLog")
    @ApiOperation("分页查询所有操作日志")
    @SysLog("分页查询所有操作日志")
    public R pageList(@ApiParam("当前页")
                      @RequestParam(value = "page",defaultValue = "1") int page,
                      @ApiParam("每页显示多少条")
                      @RequestParam(value = "size",defaultValue = "5") int size){
        Map<String, Object> stringObjectMap = sysOperateLogService.pageList(page, size);
        return R.buildSuccess(stringObjectMap);
    }

    @PostMapping("/pageListMutipleConditions")
    @ApiOperation("多条件分页查询操作日志")
    @SysLog("多条件分页查询操作日志")
    public R pageListByUsername(
            @ApiParam("多条件分页查询操作日志对象")
            @RequestBody PageListToOperateLogRequest pageListToOperateLogRequest){
        if (pageListToOperateLogRequest==null){
            return R.buildResult(BizCodeEnum.PARAM_ERROR);
        }
        Map<String, Object> stringObjectMap = sysOperateLogService.pageListToOperateLogRequest(pageListToOperateLogRequest);
        return R.buildSuccess(stringObjectMap);
    }
}
