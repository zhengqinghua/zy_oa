package com.hnxxxy.oa.system.request;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * @author ZHOUSHILIN
 * @data 2022 - 05 - 05 - 23:20
 */
@ToString
@ApiModel(value = "MenuUpdateRequest",description = "修改菜单请求对象")
@Data
public class MenuUpdateRequest implements Serializable {

    private static final long serialVersionUID = -1416388334170214183L;

    /**
     * 菜单节点id
     */
    @ApiModelProperty("菜单节点id")
    private Integer id;

    /**
     * 菜单名称
     */
    @ApiModelProperty("菜单名称")
    private String menuName;

    /**
     * 父节点id，一级菜单为0
     */
    @ApiModelProperty("父节点id，一级菜单为0")
    private Integer parentId;

    /**
     * 菜单链接地址
     */
    @ApiModelProperty("菜单链接地址")
    private String url;

    /**
     * 菜单显示顺序
     */
    @ApiModelProperty("菜单显示顺序")
    private Integer menuOrder;

    /**
     * 授权（多个用,分隔，如user:list,user:create）
     */
    @ApiModelProperty("授权标识")
    private String permission;

    /**
     * 菜单类型（0：目录、1：菜单、2：按钮）
     */
    @ApiModelProperty("菜单类型（0：目录、1：菜单、2：按钮）")
    private Integer type;

}