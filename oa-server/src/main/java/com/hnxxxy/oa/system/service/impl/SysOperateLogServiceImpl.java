package com.hnxxxy.oa.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hnxxxy.oa.system.VO.SysOperateLogVO;
import com.hnxxxy.oa.system.mapper.SysOperateLogMapper;
import com.hnxxxy.oa.system.model.SysOperateLogDO;
import com.hnxxxy.oa.system.request.PageListToOperateLogRequest;
import com.hnxxxy.oa.system.service.SysOperateLogService;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
* @author 周石林
* @description 针对表【sys_operate_log(操作日志表)】的数据库操作Service实现
* @createDate 2022-05-03 15:33:13
*/
@Service
@Slf4j
public class SysOperateLogServiceImpl implements SysOperateLogService{

    @Resource
    private SysOperateLogMapper sysOperateLogMapper;

    @Override
    public void insert(SysOperateLogDO sysLog) {
        sysOperateLogMapper.insert(sysLog);
    }

    @Override
    public Map<String, Object> pageList(int page, int size) {
        Page<SysOperateLogDO> pageInfo = new Page<>(page,size);
        Page<SysOperateLogDO> sysOperateLogDOPage = sysOperateLogMapper.selectPage(pageInfo,null);
        Map<String,Object> pageMap = new HashMap<>(3);
        pageMap.put("total_record",sysOperateLogDOPage.getTotal());
        pageMap.put("total_page",sysOperateLogDOPage.getPages());
        pageMap.put("current_data",sysOperateLogDOPage.getRecords().stream()
                .map(obj->beanProocess(obj)).collect(Collectors.toList()));
        return pageMap;
    }

    @Override
    public Map<String, Object> pageListToOperateLogRequest(PageListToOperateLogRequest pageListToOperateLogRequest) {
        LocalDateTime time = pageListToOperateLogRequest.getTime();
        String username = pageListToOperateLogRequest.getUsername();
        String remark = pageListToOperateLogRequest.getRemark();
        Integer page = pageListToOperateLogRequest.getPage();
        Integer size = pageListToOperateLogRequest.getSize();
        if (page<=0){
            page=1;
        }
        if (size<=0){
            size=10;
        }
        QueryWrapper<SysOperateLogDO> queryWrapper = new QueryWrapper<>();
        if (time!=null){
            //转换时间格式
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String format = dateTimeFormatter.format(time);
            queryWrapper.like("operate_time",format+"%");
        }
        queryWrapper.like(!Strings.isBlank(remark),"remark","%"+remark+"%");
        queryWrapper.like(!Strings.isBlank(username),"username","%"+username+"%");
        Page<SysOperateLogDO> pageInfo = new Page<>(page,size);
        Page<SysOperateLogDO> sysOperateLogDOPage = sysOperateLogMapper.selectPage(pageInfo, queryWrapper);
        Map<String,Object> pageMap = new HashMap<>(3);
        pageMap.put("total_record",sysOperateLogDOPage.getTotal());
        pageMap.put("total_page",sysOperateLogDOPage.getPages());
        pageMap.put("current_data",sysOperateLogDOPage.getRecords().stream()
                .map(obj->beanProocess(obj)).collect(Collectors.toList()));
        return pageMap;
    }

    private SysOperateLogVO beanProocess(SysOperateLogDO obj) {
        SysOperateLogVO operateLogVO = new SysOperateLogVO();
        BeanUtils.copyProperties(obj,operateLogVO);
        return operateLogVO;
    }
}




