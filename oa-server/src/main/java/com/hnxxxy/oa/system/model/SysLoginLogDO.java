package com.hnxxxy.oa.system.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import lombok.Data;

/**
 * 登录日志表
 * @TableName sys_login_log
 */
@TableName(value ="sys_login_log")
@Data
public class SysLoginLogDO implements Serializable {
    /**
     * 登录日志id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 登录者
     */
    private Integer userId;

    /**
     * 用户名
     */
    private String username;

    /**
     * 登录时间
     */
    private LocalDateTime time;

    /**
     * 登录是否成功(1：成功、0失败)
     */
    private Integer isSuccess;

    /**
     * IP地址
     */
    private String ip;

    /**
     * 登录备注
     */
    private String remark;

    /**
     * 登录时返回数据
     */
    private String message;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}