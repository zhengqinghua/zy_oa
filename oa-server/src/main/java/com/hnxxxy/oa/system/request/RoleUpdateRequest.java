package com.hnxxxy.oa.system.request;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author ZHOUSHILIN
 * @data 2022 - 05 - 05 - 13:48
 */
@ToString
@Data
@ApiModel(value = "RoleUpdateRequest",description = "修改角色请求对象")
public class RoleUpdateRequest implements Serializable {

    private static final long serialVersionUID = 2316779035054298026L;

    /**
     * 角色Id
     */
    @ApiModelProperty(value = "角色Id")
    private Integer id;

    /**
     * 角色名称
     */
    @ApiModelProperty(value = "角色名称")
    private String roleName;

    /**
     * 角色权限字符串
     */
    @ApiModelProperty(value = "角色权限字符串")
    private String roleKey;

}