package com.hnxxxy.oa.system.controller;

import com.hnxxxy.oa.annotation.SysLog;
import com.hnxxxy.oa.enums.BizCodeEnum;
import com.hnxxxy.oa.system.request.MenuAddRequest;
import com.hnxxxy.oa.system.request.MenuUpdateRequest;
import com.hnxxxy.oa.system.service.SysMenuService;
import com.hnxxxy.oa.system.service.SysRoleMenuService;
import com.hnxxxy.oa.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author ZHOUSHILIN
 * @data 2022 - 05 - 05 - 22:38
 */
@RestController
@RequestMapping("/api/system/v1")
@Api(value = "SysMenuController",tags = "菜单接口")
@Slf4j
public class SysMenuController {

    @Resource
    private SysMenuService sysMenuService;

    @Resource
    private SysRoleMenuService sysRoleMenuService;

    @PostMapping("/addMenu")
    @ApiOperation("添加菜单")
    @SysLog("添加菜单")
    public R addMenu(@RequestBody MenuAddRequest menuAddRequest) {
        if (menuAddRequest==null){
            return R.buildResult(BizCodeEnum.PARAM_ERROR);
        }
        String menuName = menuAddRequest.getMenuName();
        String url = menuAddRequest.getUrl();
        Integer parentId = menuAddRequest.getParentId();
        String permission = menuAddRequest.getPermission();
        if (StringUtils.isBlank(menuName)){
            return R.buildResult(BizCodeEnum.MENU_NAME_NULL);
        }
        if(StringUtils.isBlank(url)){
            return R.buildResult(BizCodeEnum.MENU_URL_NULL);
        }
        if (StringUtils.isBlank(permission)){
            return R.buildResult(BizCodeEnum.PERMISSTIONS_NULL);
        }
        Integer menuOrder = menuAddRequest.getMenuOrder();
        Integer type = menuAddRequest.getType();
        //校验参数
        if(menuOrder<0){
            log.info("menu order should greater than or equal to 0");
            return R.buildResult(BizCodeEnum.PARAM_ERROR);
        }
        if(type<0||type>2){
            log.info("menu type should greater than or equal to 0 and less than or equal to 2");
            return R.buildResult(BizCodeEnum.PARAM_ERROR);
        }
        if(parentId<0){
            log.info("parent menu id should greater than or equal to 0");
            return R.buildResult(BizCodeEnum.PARAM_ERROR);
        }
        Integer menuId = sysMenuService.addMenu(menuAddRequest);
        return R.buildSuccess(menuId);
    }


    @PostMapping("/updateMenu")
    @ApiOperation("修改菜单")
    @SysLog("修改菜单")
    public R updateMenu(@RequestBody MenuUpdateRequest menuUpdateRequest){
        if (menuUpdateRequest==null){
            return R.buildResult(BizCodeEnum.PARAM_ERROR);
        }
        String menuName = menuUpdateRequest.getMenuName();
        String url = menuUpdateRequest.getUrl();
        Integer parentId = menuUpdateRequest.getParentId();
        Integer id = menuUpdateRequest.getId();
        Integer menuOrder = menuUpdateRequest.getMenuOrder();
        Integer type = menuUpdateRequest.getType();
        if (StringUtils.isBlank(menuName)){
            return R.buildResult(BizCodeEnum.MENU_NAME_NULL);
        }
        if(StringUtils.isBlank(url)){
            return R.buildResult(BizCodeEnum.MENU_URL_NULL);
        }
        if(StringUtils.isAnyBlank(menuName)){
            return R.buildResult(BizCodeEnum.PARAM_NULL);
        }
        if(id==null||menuOrder==null||parentId==null){
            return R.buildResult(BizCodeEnum.PARAM_NULL);
        }
        if(id<=0||menuOrder<0||parentId<0||type<0||type>2){
            return R.buildResult(BizCodeEnum.PARAM_ERROR);
        }
        boolean b = sysMenuService.updateMenu(menuUpdateRequest);
        return R.buildSuccess(b);
    }

    @PostMapping("/deleteMenu")
    @ApiOperation("删除菜单")
    @SysLog("删除菜单")
    public R deleteMenu(@ApiParam(value = "菜单Id",required = true)
                                    @RequestBody Integer id){
        if(id==null){
            return R.buildResult(BizCodeEnum.PARAM_NULL);
        }
        if(id<=0){
            return R.buildResult(BizCodeEnum.PARAM_ERROR);
        }
        boolean b = sysMenuService.deleteMenu(id);
        //删除有该权限的用户
        sysRoleMenuService.deleteGrantAuthorzation(null,id);
        return R.buildSuccess(b);
    }

    @PostMapping("/listWithTree")
    @ApiOperation("树形列出所有菜单")
    @SysLog("树形列出所有菜单菜单")
    public R listWithTree(){
        return R.buildSuccess(sysMenuService.listWithTree());
    }


    @PostMapping("/getList")
    @ApiOperation("列出所有菜单")
    @SysLog("列出所有菜单菜单")
    public R getList(){
        return R.buildSuccess(sysMenuService.getList());
    }

}