package com.hnxxxy.oa.system.request;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author ZHOUSHILIN
 * @data 2022 - 05 - 03 - 15:52
 */
@ToString
@Data
@ApiModel(value = "UserRegisterRequest",description = "用户注册对象")
public class UserRegisterRequest implements Serializable {

    private static final long serialVersionUID = 1990671976032585518L;

    /**
     * 用户实际名字
     */
    @ApiModelProperty("实际名字")
    private String actualName;

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    private String username;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;

    /**
     * 确认密码
     */
    @ApiModelProperty(value = "确认密码")
    private String checkPassword;

    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱")
    private String email;

    /**
     * 性别
     */
    @ApiModelProperty(value = "性别")
    private Integer gender;

    /**
     * 角色id
     */
    @ApiModelProperty(value = "角色id")
    private Integer roleId;
}