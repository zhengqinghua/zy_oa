package com.hnxxxy.oa.system.controller;

import com.hnxxxy.oa.annotation.SysLog;
import com.hnxxxy.oa.enums.BizCodeEnum;
import com.hnxxxy.oa.system.request.DistributeRoleRequest;
import com.hnxxxy.oa.system.service.SysUserRoleService;
import com.hnxxxy.oa.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author ZHOUSHILIN
 * @data 2022 - 05 - 05 - 16:44
 */
@Api(value = "SysUserRoleController",tags = "用户角色接口")
@RestController
@RequestMapping("/api/system/v1")
public class SysUserRoleController {

    @Resource
    private SysUserRoleService sysUserRoleService;

    @PostMapping("/DistributeRole")
    @ApiOperation(value = "分配角色")
    @SysLog("分配角色")
    public R distributeRole(@RequestBody DistributeRoleRequest distributeRoleRequest){
        if (distributeRoleRequest==null){
            return R.buildResult(BizCodeEnum.PARAM_ERROR);
        }
        Integer roleId = distributeRoleRequest.getRoleId();
        Integer userId = distributeRoleRequest.getUserId();
        if(userId==null){
            return R.buildResult(BizCodeEnum.PARAM_NULL);
        }
        if(roleId==null){
            return R.buildResult(BizCodeEnum.PARAM_NULL);
        }
        Integer integer = sysUserRoleService.addUserRole(userId, roleId);
        return R.buildSuccess(integer);
    }

    @PostMapping("/deleteUserRole")
    @ApiOperation("删除分配角色")
    @SysLog("删除分配角色")
    public R deleteRole(@ApiParam(value = "用户id",required = true)
                            @RequestParam("userId")Integer userId,@ApiParam(value = "角色id",required = true)
    @RequestParam("roleId") Integer roleId){
        if (userId==null || roleId==null){
            return R.buildResult(BizCodeEnum.PARAM_ERROR);
        }
        boolean b = sysUserRoleService.deleteUserRole(userId, roleId);
        if (b){
            return R.buildSuccess(b);
        }
        return R.buildResult(BizCodeEnum.USER_ROLE_UNEXIST);
    }
}