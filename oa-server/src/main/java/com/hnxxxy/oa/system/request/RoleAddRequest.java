package com.hnxxxy.oa.system.request;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.management.relation.RoleStatus;
import java.io.Serializable;

/**
 * @author ZHOUSHILIN
 * @data 2022 - 05 - 05 - 10:37
 */
@ToString
@Data
@ApiModel(value = "RoleAddRequest",description = "角色添加对象")
public class RoleAddRequest implements Serializable {

    private static final long serialVersionUID = -5601925911795729086L;

    /**
     * 角色名称
     */
    @ApiModelProperty(value = "角色名称")
    private String roleName;

    /**
     * 角色权限字符串
     */
    @ApiModelProperty(value = "角色权限字符串")
    private String roleKey;

}