package com.hnxxxy.oa.system.mapper;

import com.hnxxxy.oa.system.model.SysUserDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 周石林
* @description 针对表【sys_user(用户)】的数据库操作Mapper
* @createDate 2022-05-13 16:00:15
* @Entity com.hnxxxy.oa.system.model.SysUserDO
*/
public interface SysUserMapper extends BaseMapper<SysUserDO> {

}




