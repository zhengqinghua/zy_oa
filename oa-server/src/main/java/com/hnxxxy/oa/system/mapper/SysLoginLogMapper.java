package com.hnxxxy.oa.system.mapper;

import com.hnxxxy.oa.system.model.SysLoginLogDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 周石林
* @description 针对表【sys_login_log(登录日志表)】的数据库操作Mapper
* @createDate 2022-05-15 22:21:45
* @Entity com.hnxxxy.oa.system.model.SysLoginLogDO
*/
public interface SysLoginLogMapper extends BaseMapper<SysLoginLogDO> {

}




