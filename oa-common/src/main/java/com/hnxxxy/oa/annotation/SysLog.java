package com.hnxxxy.oa.annotation;

import java.lang.annotation.*;

/**
 * 系统日志注解
 * @author ZHOUSHILIN
 * @data 2022 - 05 - 14 - 9:56
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysLog {

    String value() default "";

}