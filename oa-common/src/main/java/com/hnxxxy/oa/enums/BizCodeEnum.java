package com.hnxxxy.oa.enums;

import lombok.Getter;

/**
 * 枚举类，统一状态码和错误信息
 * @description: 状态码定义约束，共6位数，前三位代表服务，后3位代表接口
 * 比如 账号服务211,角色是212、菜单213，403代表权限
 * @author: zqh
 * @date: 2022/1/10 2:40
 **/
public enum BizCodeEnum {


    /**
     * 通用操作码
     */
    OPS_REPEAT(110001,"重复操作"),


    /**
     * 账号
     */
    ACCOUNT_REPEAT(250001,"用户名已经存在"),
    ACCOUNT_UNREGISTER(250002,"账号不存在"),
    ACCOUNT_PWD_ERROR(250003,"账号或者密码错误"),
    ACCOUNT_UNLOGIN(250004,"账号未登录"),
    LANDED_SUCCESSFULLY(250005,"账号登录成功"),
    ACCOUNT_CANCELLATION(250006,"账号注销"),
    PASSWORD_DIFFERENT(250007,"两次密码不一致"),
    PARAM_ERROR(400000,"请求参数异常"),
    PARAM_NULL(400001,"请求参数不能为空"),

    /**
     * 消息
     */
    FAILED_TO_ADD_MESSAGE(501001,"新增消息失败"),
    FAILED_TO_DELETE_MESSAGE(501002,"删除消息失败"),
    MESSAGE_DOES_NOT_EXIST(501003,"消息不存在"),
    FAILED_TO_PUBLISH_MESSAGE(501004,"消息发送失败"),




    /**
     * 系统模块状态码
     */


    /**
     * 角色
     */
    ROLE_REPEAT(212001,"角色已存在"),
    ROLE_NOTEXISTS(212002,"角色不存在"),
    ROLE_MENU_EXIST(212004,"角色已有该权限"),
    ROLE_MENU_UNEXIST(212005,"角色没有该权限"),

    /**
     * 菜单
     */
    MENU_EXIST(213001,"菜单已存在"),
    MENU_UNEXIST(213002,"菜单不存在"),
    PARENTCATALOGUE_UNEXIST(213003,"父级目录不存在"),
    PARENTMENU_UNEXIST(213003,"父级菜单不存在"),
    MENU_NAME_REPEAT(213004,"菜单名已存在"),
    BUTTON_REPEAT(213005,"按钮已存在"),
    DIRECTORY_REPEAT(213006,"目录已存在"),
    DIRECTORY_UNEXIST(213007,"目录不存在"),
    MENU_URL_NULL(213008,"菜单URL不能为空"),
    MENU_NAME_NULL(213009,"菜单名称不能为空"),
    SUPERIORMENU_NULL(213010,"上级菜单不能为空"),
    PERMISSTIONS_NULL(213011,"请选择权限"),


    /**
     * 用户
     */
    USER_ROLE_EXIST(214001,"用户已授权该角色"),
    USER_ROLE_UNEXIST(214002,"用户没有有该角色"),

    // token错误
    TOKEN_ERROR(2000, "token错误"),
    NO_TOKEN_ERROR(2001, "没有token，请重新登录"),
    USER_PASSWORD_ERROR(2002, "用户名和密码错误，请重新输入"),
    USER_UPDATE_PASS_ERROR(2005, "当前密码输入错误，请重新输入"),
    USER_UPDATE_PASS2_ERROR(2006, "两次密码输入不一致，请重新输入"),
    USER_LOCKED_ERROR(2003, "用户已锁定"),
    USER_NOROLE_LOCKED_ERROR(2004, "没有角色"),
    USER_NOPERMISSION_LOCKED_ERROR(2005, "没有权限"),





    ;


    // 权限校验
    @Getter
    private String message;

    @Getter
    private int code;

    private BizCodeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
