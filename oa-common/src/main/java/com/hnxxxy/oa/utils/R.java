package com.hnxxxy.oa.utils;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.hnxxxy.oa.enums.BizCodeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: zqh
 * @date: 2022/1/10 11:20
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class R {

    /**
     * 状态码 0 表示成功，1表示处理中，-1表示失败
     */

    private Integer code;
    /**
     * 数据
     */
    private Object data;
    /**
     * 描述
     */
    private String msg;


    /**
     * 获取远程调用数据
     * 注意事项：
     *      支持多单词下划线专驼峰 （序列化和反序列化）
     * @param typeReference
     * @param <T>
     * @return
     */
    public <T> T getData(TypeReference<T> typeReference){
        return JSON.parseObject(JSON.toJSONString(data), typeReference);
    }


    /**
     * 成功，不传入数据
     * @return
     */
    public static R buildSuccess() {
        return new R(0, null, null);
    }

    /**
     *  成功，传入数据
     * @param data
     * @return
     */
    public static R buildSuccess(Object data) {
        return new R(0, data, null);
    }

    /**
     * 失败，传入描述信息
     * @param msg
     * @return
     */
    public static R buildError(String msg) {
        return new R(-1, null, msg);
    }


    /**
     * 自定义状态码和错误信息
     * @param code
     * @param msg
     * @return
     */
    public static R buildCodeAndMsg(int code, String msg) {
        return new R(code, null, msg);
    }

    /**
     * 传入枚举，返回信息
     * @param codeEnum
     * @return
     */
    public static R buildResult(BizCodeEnum codeEnum){
        return R.buildCodeAndMsg(codeEnum.getCode(),codeEnum.getMessage());
    }
}

