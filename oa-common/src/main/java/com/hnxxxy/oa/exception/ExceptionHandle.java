package com.hnxxxy.oa.exception;


import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.exception.NotPermissionException;
import cn.dev33.satoken.exception.NotRoleException;
import com.hnxxxy.oa.enums.BizCodeEnum;
import com.hnxxxy.oa.utils.R;
import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * @description: 异常处理
 * @author: zqh
 * @date: 2022/1/10 11:28
 **/
@ControllerAdvice
@Slf4j
public class ExceptionHandle {

    /**
     * 全局异常
     * @param e
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public R Handle(Exception e) {
        //是不是自定义异常
        if (e instanceof BizException) {
            BizException bizException = (BizException) e;
            log.info("[业务异常]{}", e);
            return R.buildError(bizException.getMsg());

        } else {
            log.info("[系统异常]{}", e);
            return R.buildError("全局异常，未知错误");
        }

    }

    /**
     * token相关异常
     * @param notLoginException
     * @return
     */
    @ExceptionHandler(NotLoginException.class)
    @ResponseBody
    public R handlerNotLoginException(NotLoginException notLoginException) {
        log.warn("handlerNotLoginException [{}]", notLoginException.toString());
        // 打印堆栈，以供调试
        notLoginException.printStackTrace();
        // 判断场景值，定制化异常信息
        String message = "";
        switch (notLoginException.getType()) {
            case NotLoginException.NOT_TOKEN:
                // message = NotLoginException.NOT_TOKEN_MESSAGE;
                message = BizCodeEnum.NO_TOKEN_ERROR.getMessage();
                break;
            case NotLoginException.INVALID_TOKEN:
                message = NotLoginException.INVALID_TOKEN_MESSAGE;
                // message = BizCodeEnum.TOKEN_ERROR.getMessage();
                break;
            case NotLoginException.TOKEN_TIMEOUT:
                message = NotLoginException.TOKEN_TIMEOUT_MESSAGE;
                break;
            case NotLoginException.BE_REPLACED:
                message = NotLoginException.BE_REPLACED_MESSAGE;
                break;
            case NotLoginException.KICK_OUT:
                message = NotLoginException.KICK_OUT_MESSAGE;
                break;
            default:
                message = NotLoginException.DEFAULT_MESSAGE;
                break;
        }
        return R.buildError(message);
    }


    /**
     * 没有角色
     *
     * @param exception
     * @return
     */
    @ResponseBody
    @ExceptionHandler(NotRoleException.class)
    public R handlerNotRoleException(NotRoleException exception) {
        log.warn("handlerNotRoleException [{}]", exception.toString());
        return R.buildError("无此角色：" + exception.getRole());
    }

    /**
     * 没有权限
     *
     * @param exception
     * @return
     */
    @ResponseBody
    @ExceptionHandler(NotPermissionException.class)
    public R handlerNotPermissionException(NotPermissionException exception) {
        log.warn("handlerNotPermissionException [{}]", exception.toString());
        // return R.restResult(AdminApiErrorCode.USER_NOPERMISSION_LOCKED_ERROR, exception.getMessage());
        return R.buildError("无此权限：" + exception.getCode());
    }

}
