package com.hnxxxy.oa.exception;

import com.hnxxxy.oa.enums.BizCodeEnum;
import lombok.Data;


/**
 * @description:全局异常处理
 * @author: zqh
 * @date: 2022/1/10 11:26
 **/
@Data
public class BizException extends RuntimeException {

    private Integer code;
    private String msg;

    public BizException(Integer code, String message) {
        super(message);
        this.code = code;
        this.msg = message;
    }

    public BizException(BizCodeEnum bizCodeEnum) {
        super(bizCodeEnum.getMessage());
        this.code = bizCodeEnum.getCode();
        this.msg = bizCodeEnum.getMessage();
    }

}
